package cs224n.wordaligner; 

import cs224n.util.*;
import java.util.List;

/**
 * Simple word alignment baseline model that maps source positions to target 
 * positions along the diagonal of the alignment grid.
 * 
 * IMPORTANT: Make sure that you read the comments in the
 * cs224n.wordaligner.WordAligner interface.
 * 
 * @author Dan Klein
 * @author Spence Green
 */
public class PMIAligner implements WordAligner {

    private static final long serialVersionUID = 1315751943476440515L;
    
    // TODO: Use arrays or Counters for collecting sufficient statistics
    // from the training data.
    private CounterMap<String,String> bigramProbs;
    private Counter<String> srcProbs;
    private Counter<String> tgtProbs;
    private  double smoothing;

    public Alignment align(SentencePair sentencePair) {
    // Placeholder code below. 
    // TODO Implement an inference algorithm for Eq.1 in the assignment
    // handout to predict alignments based on the counts you collected with train().
    Alignment alignment = new Alignment();

    // YOUR CODE HERE

    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    for (int srcIdx = 0; srcIdx<sourceWords.size(); ++srcIdx){
	double maxProb = 0.0;
	int bestIdx = -1;
	for (int tgtIdx = 0; tgtIdx<targetWords.size(); ++tgtIdx){
	    double prob = bigramProbs.getCount(sourceWords.get(srcIdx), targetWords.get(tgtIdx));
	    prob /= tgtProbs.getCount(targetWords.get(tgtIdx))*srcProbs.getCount(sourceWords.get(srcIdx));
	    if (prob > maxProb){
		maxProb = prob;
		bestIdx = tgtIdx;
	    }	 
	}
	alignment.addPredictedAlignment(srcIdx, bestIdx);	
    }
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
      // YOUR CODE HERE


      this.srcProbs = new Counter<String>();
      this.tgtProbs = new Counter<String>();
      this.bigramProbs = new CounterMap<String, String>();
      smoothing = 1.0; //1.0 corresponds to no smoothing, 2.0 to Laplace, etc
      for (SentencePair sentencepair:trainingPairs){
	  List<String> sourceWords = sentencepair.getSourceWords();
	  List<String> targetWords = sentencepair.getTargetWords();
	  for (int srcIdx = 0; srcIdx<sourceWords.size(); ++srcIdx){
	      //	      System.out.println("--------=-");
	      String src_word = sourceWords.get(srcIdx);
	      //double curCount = srcProbs.getCount(src_word);
	      srcProbs.incrementCount(sourceWords.get(srcIdx),smoothing);
	      for (int tgtIdx = 0; tgtIdx<targetWords.size(); ++tgtIdx){
		  String tgt_word = targetWords.get(tgtIdx);
		  if(srcIdx == 0){
		      //curCount = tgtProbs.getCount(tgt_word);
		      //tgtProbs.setCount(tgt_word, curCount + smoothing);
		      tgtProbs.incrementCount(targetWords.get(tgtIdx),smoothing);
		  }
		  bigramProbs.incrementCount(src_word,tgt_word, smoothing);
	      }
	      
	  }
      }
  }
}
