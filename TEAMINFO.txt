icaswell (Isaac Caswell)
Wrote most of the code for PMI, IBM1, IBM2.
Contributed featurizers for Phrasal.
Wrote some motivations for featurizers as well as error analysis on Phrasal in the report.

onkursen (Onkur Sen)
Wrote helper code for, debugged AER issues in, and optimized IBM1 and IBM2.
Fixed errors in featurizers and ran experimental trials for Phrasal.
Wrote descriptions and most motivations for featurizers as well as error analysis for PMI, IBM1, and IBM2.
