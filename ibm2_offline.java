package cs224n.wordaligner;

import cs224n.util.*;
import java.util.List;
import java.util.HashSet;

/**                                                                                                                                                                                  
 * Simple word alignment baseline model that maps target positions to source                                                                                                         
 * positions along the diagonal of the alignment grid.                                                                                                                               
 *                                                                                                                                                                                   
 * IMPORTANT: Make sure that you read the comments in the                                                                                                                            
 * cs224n.wordaligner.WordAligner interface.                                                                                                                                         
 *                                                                                                                                                                                   
 * @author Dan Klein                                                                                                                                                                 
 * @author Spence Green                                              a                                                                                                                
 */

public class IBMModel2Aligner implements WordAligner {

  private static final long serialVersionUID = 1315751943476440515L;

  // TODO: Use arrays or Counters for collecting sufficient statistics                                                                                                               
  // from the training data.                                                                                                                                                         
  //************************************************************
  // variables:

  //t(f|e)
  //AKA targetsourceCounts
  //first argument is the source word, second the target
  private CounterMap <String, String> p_src_given_tgt = new CounterMap<String,String>(); 
  private CounterMap <int, String> q_ml = new CounterMap <int, String>(); //TODO: make it four dimensional
    //************************************************************

  public Alignment align(SentencePair sentencePair) {
    Alignment alignment = new Alignment();

    // YOUR CODE HERE                                                                                                                                                                
    List<String> targetWords = sentencePair.gettargetWords();
    List<String> sourceWords = sentencePair.getsourceWords();
    sourceWords.add("<NULL>");
    for(int tgtIdx = 0; tgtIdx < targetWords.size(); tgtIdx++){
        String tgtWord = targetWords.get(tgtIdx);
        int bestTransIdx = -1;
        double bestTransIdxVal = -1.0;
        //get the source word yielding the highest translation probability... 
        for(int srcIdx = 0; srcIdx < sourceWords.size(); srcIdx++){
            String srcWord = sourceWords.get(srcIdx);

            if (this.p_src_given_tgt.getCount(srcWord, tgtWord) > bestTransIdxVal){
                bestTransIdx = srcIdx;
                bestTransIdxVal = this.p_src_given_tgt.getCount(srcWord, tgtWord);
                if (srcWord.equals("<NULL>")){
		    //System.out.format("src %s=NULL\n", srcWord);
                    bestTransIdx = -1;
                } else {
		    //System.out.format("src %s!=NULL\n", srcWord);
		}
            }
        }
        //and add the relevant index to the alignment
        if(bestTransIdx != -1){
            // don't add the NULL alignment
            alignment.addPredictedAlignment(tgtIdx, bestTransIdx);
        }
    }
    
    return alignment;
  }

    public void train(List<SentencePair> trainingPairs) {

        //************************************************************
    	HashSet<String> srcVocab = new HashSet<String>();
    	HashSet<String> tgtVocab = new HashSet<String>();

        for (SentencePair sentencepair:trainingPairs){
    	    List<String> targetWords = sentencepair.gettargetWords();
    	    List<String> sourceWords = sentencepair.getsourceWords();
    	    for (String word:targetWords){
    		  tgtVocab.add(word);
    	    }
    	    for (String word:sourceWords){
    		  srcVocab.add(word);
	        }  

        }

        // TODO: which of the above constants to normalize by?
        double uniform_prob = 1.0/srcVocab.size();
        
        for (int i=0; i < 10; i++){
            //TODO: make this actually run until convergence

            // alignment_counts(tgtWord, srcWord) gives the pseudoprobability that srcWord and tgtWord are aligned.
            // AKA the (fractional) co-occurence counts
            // NOTE THAT THE ORDER OF ARGUMENTS IS REVERSED FROM p_src_given_tgt!
            // TODO: I want to make these on the stack! how can I avoid 'new'?
            CounterMap <String, String> alignment_counts = new CounterMap<String, String>(); 
            Counter <String> tgt_counts = new Counter<String>();

            //length_counts: c(i, l, m) from Collins
            //where the string corresponds to "%s_%s"%(l, m) == "%s_%s"%(tgt_len, src_len)
            //and the integer to i
            CounterMap <int, String> length_counts = new CounterMap<int, String>(); 

            //length_alignment_counts: c(j|i, l, m) from Collins
            //where the string corresponds to "%s_%s_%s"%(i, l, m)
            //and the integer to j
            CounterMap <int, String> length_alignment_counts = new CounterMap<int, String>(); // c(j|i, l, m)

            //TODO: initialize p, q as per https://piazza.com/class/iemvbzjzuvb1z?cid=139

            //========================================================
            // E-step : apply the model to the data.  
            // This means: get the most likely alignments
            // This means: calculate the probabilities of all m*l alignments,
            // by means of calculating, for each source word, the alignment probability of each target word
            for (SentencePair sentencepair:trainingPairs){
                List<String> targetWords = sentencepair.gettargetWords();
                List<String> sourceWords = sentencepair.getsourceWords();
                sourceWords.add("<NULL>"); //TODO: add this before or after calculating lengths?
                int len_tgt = targetWords.size();
                int len_src = sourceWords.size();
 

                //the sum of the counts of each source word.  Used for normalization
                //TODO: I want this on the stack!
                Counter<String> source_sums = new Counter<String>();
                //get normalization constants/populate (source_sums)
                for (int srcIdx=0;  srcIdx < sourceWords.size(); ++ srcIdx){
                    for (int tgtIdx=0;  tgtIdx < targetWords.size(); ++ tgtIdx){
                        String srcWord = sourceWords.get(srcIdx);
                        String tgtWord = targetWords.get(tgtIdx);
                        double t_val = p_src_given_tgt.getCount(srcWord,tgtWord); //rename p 
            			

                        String q_key = srcIdx + '_' + tgt_len + '_' + src_len; //TODO: check if it's srcIdx or tgtIdx (this line and below)
                        double q_val = length_alignment_counts.getCount(tgtIdx, q_key);
                        if (q_val == 0.0) q_val = 1.0/(tgt_len + 1);

                        //TODO: return default, depending on implementation
                        source_sums.incrementCount(srcWord, q_val*t_val);
                    }
                }               

                for (int srcIdx=0;  srcIdx < sourceWords.size(); ++ srcIdx){
                    for (int tgtIdx=0;  tgtIdx < targetWords.size(); ++ tgtIdx){
                        String srcWord = sourceWords.get(srcIdx);
                        String tgtWord = targetWords.get(tgtIdx);


            			double t_val = this.p_src_given_tgt.getCount(srcWord, tgtWord);
            			if (t_val == 0.0) t_val = uniform_prob; //TODO: remove

                        String q_key = srcIdx + '_' + tgt_len + '_' + src_len; //TODO: check if it's srcIdx or tgtIdx (this line and below)
                        double q_val = length_alignment_counts.getCount(tgtIdx, q_key);
                        
                        double count_delta = (q_val*t_val)/source_sums.getCount(srcWord);
                        alignment_counts.incrementCount(tgtWord, srcWord, count_delta);
                        tgt_counts.incrementCount(tgtWord, count_delta);
                        length_alignment_counts.getCount((j, i, len_tgt, len_src));
                        length_counts.getCount((i, len_tgt, len_src))
                    }
                }
            }
            //========================================================        
            // M-step: learn the model from the data/Renormalization
            //this means, one presumes, estimating t(f|e) for all f
	    //run over all pairs of (tgt_word, src_word) in
            // alignment_counts
	    for (String tgtWord:tgtVocab){
    		for (String srcWord:srcVocab){
                //for the translation probs:
                double joint = alignment_counts.getCount(tgtWord, srcWord);
                double prior = tgt_counts.getCount(tgtWord);
                this.p_src_given_tgt.setCount(srcWord, tgtWord, joint/prior);
    		}
	    }
        //TODO: M step for q distribution
        //iterate over all key pairs in q_ml
        }                                                                                                                                                          
    }
}




