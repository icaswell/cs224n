package cs224n.wordaligner;

import cs224n.util.*;
import java.util.List;
import java.util.Set;

/**                                                                                                                                                                                  
 * Simple word alignment baseline model that maps target positions to source
 * positions along the diagonal of the alignment grid.                                                                                                   
 *                                                                                                                                                                                   
 * IMPORTANT: Make sure that you read the comments in the                                                                                                                            
 * cs224n.wordaligner.WordAligner interface.                                                                                                                                         
 *                                                                                                                                                                                   
 * @author Dan Klein                                                                                                                                                                 
 * @author Spence Green
 */

public class IBMModel2Aligner extends IBMModel1Aligner {

  private static final long serialVersionUID = 1315751943476440515L;

  // pSrcGivenTgt is inherited from IBMModel1Aligner
  private Counter <String> distortionProbs = new Counter <String>();

  public Alignment align(SentencePair sentencePair) {
    Alignment alignment = new Alignment();

    List<String> targetWords = sentencePair.getTargetWords();
    List<String> sourceWords = sentencePair.getSourceWords();
    int lenTgt = targetWords.size();
    int lenSrc = sourceWords.size();

    targetWords.add("<NULL>");
    for(int tgtIdx = 0; tgtIdx < targetWords.size(); tgtIdx++){
        String tgtWord = targetWords.get(tgtIdx);
        int bestTransIdx = -1;
        double bestTransProb = -1.0;

        //get the source word yielding the highest translation probability... 
        for(int srcIdx = 0; srcIdx < sourceWords.size(); srcIdx++){
            String srcWord = sourceWords.get(srcIdx);
            double tVal = this.pSrcGivenTgt.getCount(tgtWord, srcWord);
            String qKey = tgtIdx + "___" + srcIdx + "___" + lenTgt + "___" + lenSrc;
            double qVal = this.distortionProbs.getCount(qKey);
            double prob = tVal * qVal;
            if (prob > bestTransProb){
                if (tgtWord.equals("<NULL>")) {
                    bestTransIdx = -1;
                } else {
		    bestTransIdx = srcIdx;
		    bestTransProb = prob;
		}
            }
        }
        //and add the relevant index to the alignment
        // don't add the NULL alignment
        if(bestTransIdx != -1) {
            alignment.addPredictedAlignment(tgtIdx, bestTransIdx);
        }
    }
    
    return alignment;
  }

    public void train(List<SentencePair> trainingPairs) {

        //************************************************************
        //train IBM1 to get initializations for translation probs
        super.train(trainingPairs);
        //************************************************************

        for (int i=0; i < 20; i++){
            //TODO: make this actually run until convergence

            // alignment_counts(tgtWord, srcWord) gives the pseudoprobability that srcWord and tgtWord are aligned.
            // AKA the (fractional) co-occurence counts
            // NOTE THAT THE ORDER OF ARGUMENTS IS REVERSED FROM pSrcGivenTgt!
            // TODO: I want to make these on the stack! how can I avoid 'new'?
            CounterMap <String, String> alignment_counts = new CounterMap<String, String>(); 
            Counter <String> tgt_counts = new Counter<String>();

            //length_counts: c(i, l, m) from Collins
            //where the string corresponds to 
            //tgt_Idx + "___" + srcIdx + "___" + lenTgt + "___" + lenSrc;
            //AKA
            //i + "___" + lenTgt + "___" + lenSrc;
            Counter <String> length_counts = new Counter<String>(); 

            //length_alignment_counts: c(j|i, l, m) from Collins
            //where the string corresponds to 
            //tgt_Idx + "___" + srcIdx + "___" + lenTgt + "___" + lenSrc;
            //AKA
            //j + "___" + i + "___" + lenTgt + "___" + lenSrc;
            Counter <String> length_alignment_counts = new Counter<String>(); // c(j|i, l, m)

            //========================================================

            // E-step : apply the model to the data.  
            // This means: get the most likely alignments
            // This means: calculate the probabilities of all m*l alignments,
            // by means of calculating, for each source word, the alignment probability of each target word
            for (SentencePair sentencepair:trainingPairs){
                List<String> targetWords = sentencepair.getTargetWords();
                List<String> sourceWords = sentencepair.getSourceWords();
                int lenTgt = targetWords.size();
                int lenSrc = sourceWords.size();
                targetWords.add("<NULL>"); 
 

                // //=================================================================
                // // get the sum of the counts of each source word.  Used for normalization
                // Counter<String> source_sums = new Counter<String>();
                // //get normalization constants/populate (source_sums)
                // for (int srcIdx=0;  srcIdx < sourceWords.size(); ++ srcIdx){
                //     for (int tgtIdx=0;  tgtIdx < targetWords.size(); ++ tgtIdx){
                //         String srcWord = sourceWords.get(srcIdx);
                //         String tgtWord = targetWords.get(tgtIdx);

                //         String wordPairKey = srcWord + "___" + tgtWord;  
                //         String fullQKey = tgtIdx + "___" + srcIdx + "___" + lenTgt + "___" + lenSrc;                      

                //         //------------------------------------------------------
                //         // BEGIN q(j|i, l, m)*t(f|i)
                //         double tVal = this.pSrcGivenTgt.getCount(wordPairKey);
                //         double qVal = this.distortionProbs.getCount(fullQKey);
                //         //set to default if it hasn't been initialized
                //         if (qVal == 0.0) qVal = 1.0/(lenTgt + 1.0);
                //         // END q(j|i, l, m)*t(f|i)                      
                //         //------------------------------------------------------

                //         source_sums.incrementCount(srcWord, qVal*tVal);
                //     }
                // }   

                //=================================================================
                // get fractional counts
                for (int srcIdx=0;  srcIdx < sourceWords.size(); ++ srcIdx){
                    //---------------------------------------------------------
                    // get normalization constant
                    double normalizationConstant = 0.0;
                    for (int tgtIdx=0;  tgtIdx < targetWords.size(); ++ tgtIdx){
                        String srcWord = sourceWords.get(srcIdx);
                        String tgtWord = targetWords.get(tgtIdx);
                        String fullQKey = tgtIdx + "___" + srcIdx + "___" + lenTgt + "___" + lenSrc;                      

                        //------------------------------------------------------
                        // BEGIN q(j|i, l, m)*t(f|i)
                        double tVal = this.pSrcGivenTgt.getCount(tgtWord, srcWord);
                        double qVal = this.distortionProbs.getCount(fullQKey);
                        //set to default if it hasn't been initialized
                        if (qVal == 0.0) qVal = 1.0/(lenTgt + 1.0);
                        // END q(j|i, l, m)*t(f|i)
                        //------------------------------------------------------
                        normalizationConstant +=  qVal*tVal;
                    }


                    //---------------------------------------------------------
                    // Above all was the sense of hearing acute. I heard all things 
                    // in the heaven and in the earth. I heard many things in hell. 
                    // How, then, am I mad? 
                    for (int tgtIdx=0;  tgtIdx < targetWords.size(); ++ tgtIdx){
                        String srcWord = sourceWords.get(srcIdx);
                        String tgtWord = targetWords.get(tgtIdx);

                        //------------------------------------------------------
                        // make all the hacky keys
                        String wordPairKey = srcWord + "___" + tgtWord; 
                        String partialQKey = srcIdx + "___" + lenTgt + "___" + lenSrc;
                        String fullQKey = tgtIdx + "___" + partialQKey;

                        //------------------------------------------------------
                        // count_delta calculation
                        double tVal = this.pSrcGivenTgt.getCount(tgtWord, srcWord);
                        double qVal = this.distortionProbs.getCount(fullQKey);
                        if (qVal == 0.0) qVal = 1.0/(lenTgt + 1);
                        double count_delta = (qVal*tVal)/normalizationConstant;

                        // END count_delta calculation                        
                        //------------------------------------------------------

                        alignment_counts.incrementCount(tgtWord, srcWord, count_delta);
                        tgt_counts.incrementCount(tgtWord, count_delta);
                        length_alignment_counts.incrementCount(fullQKey, count_delta);
                        length_counts.incrementCount(partialQKey, count_delta);
                    }
                }
                //========================================================
                //bookkeepping: remove the NULL we inserted
                targetWords.remove(targetWords.size()-1); 
            }


            //========================================================        
            // M-step: learn the model from the data/Renormalization
            //this means, one presumes, estimating t(f|e) for all f
            //run over all pairs of (tgt_word, src_word) in
            // alignment_counts
            for (String tgtWord : pSrcGivenTgt.keySet()){
		for (String srcWord : pSrcGivenTgt.getCounter(tgtWord).keySet()){
		    double joint = alignment_counts.getCount(tgtWord, srcWord);
		    double prior = tgt_counts.getCount(tgtWord);
		    this.pSrcGivenTgt.setCount(tgtWord, srcWord, joint/prior);
		}
            }
            for (String fullKey: length_alignment_counts.keySet()){
                String[] parts = fullKey.split("___");
                String partialKey = parts[1] + "___"+ parts[2] + "___"+
                                parts[3];
                //System.out.format("----lac %s\n",length_alignment_counts.getCount(fullKey));
                double newVal = length_alignment_counts.getCount(fullKey);
                newVal /= length_counts.getCount(partialKey);
                //System.out.format("++++++++ %s    %s\n", length_alignment_counts.getCount(fullKey), length_counts.getCount(partialKey));                
                //System.out.format("  --lc %s\n",length_counts.getCount(partialKey));
                distortionProbs.setCount(fullKey, newVal);
            }
        }                                                                                                                                                          
    }
}




