package cs224n.wordaligner; 

import cs224n.util.*;
import java.util.List;

/**
 * Simple word alignment baseline model that maps source positions to target 
 * positions along the diagonal of the alignment grid.
 * 
 * IMPORTANT: Make sure that you read the comments in the
 * cs224n.wordaligner.WordAligner interface.
 * 
 * @author Dan Klein
 * @author Spence Green
 */
public class PMIAligner implements WordAligner {

    private static final long serialVersionUID = 1315751943476440515L;
    
    // TODO: Use arrays or Counters for collecting sufficient statistics
    // from the training data.
    private Counter<String> bigramProbs;
    private Counter<String> srcProbs;
    private Counter<String> tgtProbs;
    private  double smoothing;

    public Alignment align(SentencePair sentencePair) {
    // Placeholder code below. 
    // TODO Implement an inference algorithm for Eq.1 in the assignment
    // handout to predict alignments based on the counts you collected with train().
    Alignment alignment = new Alignment();

    // YOUR CODE HERE

    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    targetWords.add("<NULL>"); 
    for (int srcIdx = 0; srcIdx<sourceWords.size(); ++srcIdx){
        double maxProb = -1;
        int bestIdx = -1;
        for (int tgtIdx = 0; tgtIdx<targetWords.size(); ++tgtIdx){
            String srcWord = sourceWords.get(srcIdx);
            String tgtWord = targetWords.get(tgtIdx);
            double prob = bigramProbs.getCount(srcWord + "___" + tgtWord);
            prob /= (tgtProbs.getCount(tgtWord)*srcProbs.getCount(srcWord));
            if (prob > maxProb){
                maxProb = prob;
                System.out.format("%s, %s --> p=%s\n", srcWord, tgtWord, prob);
                bestIdx = tgtIdx;
                if (tgtWord.equals("<NULL>"))
                    bestIdx = -1;
            }     
        }
    if (bestIdx!=-1)
        alignment.addPredictedAlignment(srcIdx, bestIdx);
    }
    targetWords.remove(targetWords.size()-1); 
    return alignment;
  }

  public void train(List<SentencePair> trainingPairs) {
      // YOUR CODE HERE
      this.srcProbs = new Counter<String>();
      this.tgtProbs = new Counter<String>();
      this.bigramProbs = new Counter<String>();
      smoothing = 2.0; //1.0 corresponds to no smoothing, 2.0 to Laplace, etc
      for (SentencePair sentencepair:trainingPairs){
          List<String> sourceWords = sentencepair.getSourceWords();
          List<String> targetWords = sentencepair.getTargetWords();
          targetWords.add("<NULL>"); 
          for (int srcIdx = 0; srcIdx<sourceWords.size(); ++srcIdx){
              //          System.out.println("--------=-");
              String srcWord = sourceWords.get(srcIdx);
              //double curCount = srcProbs.getCount(srcWord);
              srcProbs.incrementCount(srcWord,smoothing);
              for (int tgtIdx = 0; tgtIdx<targetWords.size(); ++tgtIdx){
                  String tgtWord = targetWords.get(tgtIdx);
                  if(srcIdx == 0){
                      //curCount = tgtProbs.getCount(tgtWord);
                      //tgtProbs.setCount(tgtWord, curCount + smoothing);
                      tgtProbs.incrementCount(tgtWord, smoothing);
                  }
                  bigramProbs.incrementCount(srcWord + "___" + tgtWord, smoothing);
              }
          }
          targetWords.remove(targetWords.size()-1); 
      }

      //Normalize probabilities - souldn't be necessary
      double srcPartition = srcProbs.totalCount();
      double tgtPartition = tgtProbs.totalCount();
      double bigramPartition = bigramProbs.totalCount();
      System.out.print (bigramPartition);         

      for (String key: srcProbs.keySet()){
        srcProbs.setCount(key, srcProbs.getCount(key)/srcPartition);
      }
      for (String key: tgtProbs.keySet()){
        tgtProbs.setCount(key, tgtProbs.getCount(key)/tgtPartition);
      }      
    for (String key: bigramProbs.keySet()){
        bigramProbs.setCount(key, bigramProbs.getCount(key)/bigramPartition);
      }    
  }
}
