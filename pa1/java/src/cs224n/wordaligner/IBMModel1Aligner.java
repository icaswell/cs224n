package cs224n.wordaligner;

import cs224n.util.*;
import java.util.List;
import java.util.Set;

/**
 * Simple word alignment baseline model that maps target positions to source
 * positions along the diagonal of the alignment grid.
 * IMPORTANT: Make sure that you read the comments in the
 * cs224n.wordaligner.WordAligner interface.
 * @author Dan Klein
 * @author Spence Green
 */

public class IBMModel1Aligner implements WordAligner {

    private static final long serialVersionUID = 1315751943476440515L;
    private static final int NUM_RUNS_TO_CONVERGENCE = 20;

    public CounterMap<String, String> pSrcGivenTgt = new CounterMap<String, String>();
    
    public Alignment align(SentencePair sentencePair) {
	Alignment alignment = new Alignment();
    
	List<String> targetWords = sentencePair.getTargetWords();
	List<String> sourceWords = sentencePair.getSourceWords();
	targetWords.add("<NULL>");
	
	for (int tgtIdx = 0; tgtIdx < targetWords.size(); tgtIdx++) {
	    String tgtWord = targetWords.get(tgtIdx);
	    int bestTransIdx = -1;
	    double bestTransIdxVal = -1.0;
	    
	    // Get the source word yielding the highest translation probability
	    for (int srcIdx = 0; srcIdx < sourceWords.size(); srcIdx++) {
		String srcWord = sourceWords.get(srcIdx);
		double transIdxVal = pSrcGivenTgt.getCount(tgtWord, srcWord);
		if (transIdxVal > bestTransIdxVal) {
		    if (tgtWord.equals("<NULL>")) {
			bestTransIdx = -1;
		    } else {
			bestTransIdx = srcIdx;
			bestTransIdxVal = transIdxVal;
		    }
		}
	    }
	    
	    // Add the relevant index to the alignment
	    // don't add the NULL alignment
	    if (bestTransIdx != -1) {
		alignment.addPredictedAlignment(tgtIdx, bestTransIdx);
	    }
	}
	return alignment;
    }
    
    public void train(List<SentencePair> trainingPairs) {
        double uniformProb = 1.0 / (5 * trainingPairs.size());
        for (int i = 0; i < NUM_RUNS_TO_CONVERGENCE; i++) {
            // alignmentCounts(tgtWord, srcWord) gives the pseudoprobability that srcWord and tgtWord are aligned.
            // AKA the (fractional) co-occurence counts
            // NOTE THAT THE ORDER OF ARGUMENTS IS REVERSED FROM pSrcGivenTgt!
            CounterMap<String, String> alignmentCounts = new CounterMap<String, String>(); 
	    
            // E-step: apply the model to the data.  
            // This means: get the most likely alignments
            // This means: calculate the probabilities of all m*l alignments,
            // by means of calculating, for each source word, the alignment probability of each target word
            for (SentencePair sentencePair : trainingPairs) {
                List<String> targetWords = sentencePair.getTargetWords();
                List<String> sourceWords = sentencePair.getSourceWords();
                targetWords.add("<NULL>");
		
                for (String srcWord : sourceWords) {
                    if (srcWord == null || srcWord.isEmpty()) continue;
		    
                    // First get the normalization constant
                    double normalizationConstant = 0.0;
                    for (String tgtWord : targetWords) {
                        if (tgtWord == null || tgtWord.isEmpty()) continue;                      
                        double prob  = pSrcGivenTgt.getCount(tgtWord, srcWord);
                        if (prob == 0.0) {
                            prob = uniformProb;
                            pSrcGivenTgt.setCount(tgtWord, srcWord, uniformProb);
                        }      
                        normalizationConstant += prob;
                    }
		    
                    // Now this is the point. You fancy me mad. Madmen know nothing. 
                    // But you should have seen me. You should have seen how wisely I proceeded 
                    // --with what caution --with what foresight --with what dissimulation I went to work!                   
                    for (String tgtWord : targetWords) {
                        if (tgtWord == null || tgtWord.isEmpty()) continue;
			double prob = pSrcGivenTgt.getCount(tgtWord, srcWord);
			alignmentCounts.incrementCount(tgtWord, srcWord, prob / normalizationConstant);
                    }
                }
            }
	    
            // M-step: learn the model from the data/Renormalization
            // this means, one presumes, estimating t(f|e) for all f
            // run over all pairs of (tgt_word, src_word) in
            // alignmentCounts
            for (String tgtWord : pSrcGivenTgt.keySet()) {
		Set<String> srcWords = pSrcGivenTgt.getCounter(tgtWord).keySet();
		double prior = 0;
		for (String srcWord : srcWords) {
		    prior += pSrcGivenTgt.getCount(tgtWord, srcWord);
		}
		for (String srcWord : srcWords) {
		    double joint = alignmentCounts.getCount(tgtWord, srcWord);
		    pSrcGivenTgt.setCount(tgtWord, srcWord, joint / prior);
		}
            }
        }
    }
}
