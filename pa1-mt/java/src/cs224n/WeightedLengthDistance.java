package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;

public class WeightedLengthDistance implements RuleFeaturizer<IString, String> {

  public static final String FEATURE_NAME = "LRA";
  
  @Override
  public void initialize() {
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(Featurizable<IString, String> f) {
    /*
    exports two features related to the difference between lengths of source 
    and target translations:

    1. letter_diff: the difference in total number of letters in
      src and target phrase.  The idea here is that long words will tend to 
      translate to long words, short to short, etc.

    2. len_diff_over_3: absolute value of difference of length of source and target
       phrase, only counting words longer than 3.  This controls for the fact 
       that many small function words sneak in to either language, e.g. 
       'mis en application' 'was applied'

      NOTE: this is similar to length_ratio from the example featurizers.  
      Whether this approach (difference instead of ratio; etc) is better is hard to say.
    */


    double letters_in_src = 0.0;
    double letters_in_tgt = 0.0;    

    double over_3_in_src = 0.0;
    double over_3_in_tgt = 0.0;    

    //TODO: below are TK types, not Strings, so this will def fail
    for (IString srcWrd:f.sourcePhrase){
      letters_in_src += srcWrd.length();
      if (srcWrd.length() > 3){
        over_3_in_src+=1.0;
      } 
    }
    for (IString tgtWrd:f.targetPhrase){
      letters_in_tgt += tgtWrd.length();
      if (tgtWrd.length() > 3){
        over_3_in_tgt+=1.0;
      } 
    }    


    List<FeatureValue<String>> features = Generics.newLinkedList();
    features.add(new FeatureValue<String>("letter_diff", java.lang.Math.abs(letters_in_src -letters_in_tgt)));
    features.add(new FeatureValue<String>("len_diff_over_3", java.lang.Math.abs(over_3_in_src -over_3_in_tgt)));      
    //double sizeDiff = ((double) f.targetPhrase.size() - (double) f.sourcePhrase.size());
    return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}
