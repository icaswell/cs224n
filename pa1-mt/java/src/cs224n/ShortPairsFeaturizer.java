package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;

/**
 * A rule featurizer.
 */
public class ShortPairsFeaturizer implements RuleFeaturizer<IString, String> {
  
  @Override
  public void initialize() {
    // Do any setup here.
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(
      Featurizable<IString, String> f) {
    int TGT_CUTOFF = 2;
    int SRC_CUTOFF = 2;
    String FEATURE_NAME = "SHORT_PAIR_" + TGT_CUTOFF + "_" + SRC_CUTOFF;

    // TODO: Return a list of features for the rule. Replace these lines
    // with your own feature.
    List<FeatureValue<String>> features = Generics.newLinkedList();

    for(IString srcWord:f.sourcePhrase){
	for(IString tgtWord:f.targetPhrase){
	    String srcWordStr = srcWord.toString();
	    String tgtWordStr = tgtWord.toString();
	    if (srcWordStr.length() <= SRC_CUTOFF && tgtWordStr.length() <= TGT_CUTOFF) {
		features.add(new FeatureValue<String>(FEATURE_NAME + srcWordStr + "-->" + tgtWordStr, 1.0));
	    }
	}
    }
    return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}
