package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;

/**
 * A rule featurizer.
 */
public class ShortSrcWordsFeaturizer implements RuleFeaturizer<IString, String> {
  
  @Override
  public void initialize() {
    // Do any setup here.
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(
      Featurizable<IString, String> f) {
    String FEATURE_NAME = "SHORT_SRC_WORD_";

    // TODO: Return a list of features for the rule. Replace these lines
    // with your own feature.
    List<FeatureValue<String>> features = Generics.newLinkedList();

    for(IString srcWord : f.sourcePhrase) {
	String srcWordStr = srcWord.toString();
	if (srcWordStr.length() <= 2) {
	    features.add(new FeatureValue<String>(FEATURE_NAME + srcWordStr, 1.0));
	}
    }
    return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}
