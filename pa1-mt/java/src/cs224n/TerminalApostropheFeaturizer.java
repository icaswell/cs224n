package edu.stanford.nlp.mt.decoder.feat;

import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;

/**
 * A rule featurizer.
 */
public class TerminalApostropheFeaturizer implements RuleFeaturizer<IString, String> {
  
  @Override
  public void initialize() {
    // Do any setup here.
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(
      Featurizable<IString, String> f) {

    // TODO: Return a list of features for the rule. Replace these lines
    // with your own feature.
    List<FeatureValue<String>> features = Generics.newLinkedList();

    if(f.sourcePhrase.size() == 1){
	IString srcWord = f.sourcePhrase.get(0);
      if (srcWord.charAt(srcWord.length() - 1) == '\'') {
        features.add(new FeatureValue<String>("terminalApostrophe", 1.0));
      }
    }
    return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}
