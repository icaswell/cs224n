package edu.stanford.nlp.mt.decoder.feat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.stanford.nlp.mt.util.FeatureValue;
import edu.stanford.nlp.mt.util.Featurizable;
import edu.stanford.nlp.mt.util.IString;
import edu.stanford.nlp.mt.decoder.feat.RuleFeaturizer;
import edu.stanford.nlp.util.Generics;

/**
 * A rule featurizer.
 */
public class MinProbFeaturizer implements RuleFeaturizer<IString, String> {
  
  @Override
  public void initialize() {
    // Do any setup here.
  }

  @Override
  public List<FeatureValue<String>> ruleFeaturize(
      Featurizable<IString, String> f) {

      // an indicator feature that fires if the source and target start
      // with the same two letters and are of similar length
      List<FeatureValue<String>> features = Generics.newLinkedList();
      
      List<Float> translationScores = new ArrayList<Float>();
      for (float score : f.translationScores) {
	  translationScores.add(score);
      }
      features.add(new FeatureValue<String>("minProb", Collections.min(translationScores)));

      return features;
  }

  @Override
  public boolean isolationScoreOnly() {
    return false;
  }
}
