#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# File: wordvector_playground.py
# @Author: Isaac Caswell
# @Author: Allen Nie
# @created: 12 Nov 2015
#
#=========================================================================
# DESCRIPTION:
#
# takes some trained word vectors and fools around with them.  Ultimately, we want
# to compare adversarial examples to learned embeddings, and see what real sentences
# they resemble.
#
#=========================================================================
# CURRENT STATUS: contains fun stuff, not yet useful (12 Nov 2015)
#=========================================================================
# USAGE:
#  python wordvector_playground.py
#=========================================================================
# TODO
#
# --do something more elegant than nearest neighbors.  look, for instance, at the
# **direction** of the perturbation.  What word do you find next if you travel in a
# hypercone along that direction?
#
# --get real data from our models


import numpy as np
import theano
import copy
import cPickle
from sklearn.neighbors import NearestNeighbors

#=======================================================
# constants

#toy_sentence = "We will make pendants of gold for you and ornaments of silver While the king was upon his couch my spikenard gave forth its fragrance".split()

# toy_sentence type: [string]
toy_sentence = "be like a gazelle or a young stag upon the mountains of spices".split()

IDX_MAP_FPATH = "./data/toy_corpus_idx_map.pkl"  # mapping of word to index
# model has word embedding
SAVED_MODEL_FPATH = "saved_models/lstm_model_toy.npz"
np.random.seed(3)
NOISE = .011


#=======================================================
# load our variables

Wemb = None

with np.load(SAVED_MODEL_FPATH) as data:
    Wemb = data["Wemb"]

# this word embedding, (vocab_size, dim_proj)  (10000, 128)
vocab_size, dim_proj = Wemb.shape

# this loads a file that already index each word (word:index) in corpus, also has a
# label to index, which we don't use
f = open(IDX_MAP_FPATH, 'rb')
(word_to_idx, label_to_idx) = cPickle.load(f)
f.close()

# reverse map to index:word
idx_to_word = {idx: word for word, idx in word_to_idx.items()}

#=======================================================
# fit a nearest neighbors algorithm, for reference
nbrs = NearestNeighbors(n_neighbors=3, algorithm='ball_tree').fit(
    Wemb)  # memorize all examples seen so far

#=======================================================
# convert sentence to symbolic/embedded representation


# TODO: below treats UNK tokens as whatever the 0th word is, which is very
# bad.  This is good only for our toy tests.
# a list of indices which correspond to the row in the word embedding matrix
symbolic_toy_sentence = [word_to_idx.get(word, 0) for word in toy_sentence]

embedded_toy_sentence = [Wemb[w_i, :] for w_i in symbolic_toy_sentence]

#=======================================================
# create faux adversarial example by adding noise to the original word vectors

pseudo_adv_embedded_toy_sentence = copy.deepcopy(embedded_toy_sentence)
pseudo_adv_embedded_toy_sentence = [
    emb + NOISE * np.random.random(dim_proj,) for emb in pseudo_adv_embedded_toy_sentence]

#=======================================================
# decode this sentence
# distance to the nearest list of length 3 (each one corresponds to word)
distances, indices = nbrs.kneighbors(pseudo_adv_embedded_toy_sentence)

decoded_symbolic_pseudo_adv_embedded_toy_sentence = [
    neighbor_indices[0] for neighbor_indices in indices]


# below adding of UNK: we shouldn't actually need to check for this. There
# is some problem.
decoded_pseudo_adv_embedded_toy_sentence = [idx_to_word.get(
    w_i, "<UNK>") for w_i in decoded_symbolic_pseudo_adv_embedded_toy_sentence]


#=======================================================
# print the old and the new decoded sentence

print "original sentence:"
print "\t", " ".join(toy_sentence)

print "sentence decoded after adding random noise:"
print "\t", " ".join(decoded_pseudo_adv_embedded_toy_sentence)
