#!usr/bin/bash
python rnn.py --id deadweek_cpu --redirect --optimizer adadelta --clip 5 --wdim 128 --batch_size 16 --encoder lstm --epochs 500 --data imdb --hdim 128 --weight-init ortho_1.0 --maxlen 100 --reg 0.0001 --wemb_init random \
--adv 1 --alpha 0.25 --eps 0.4