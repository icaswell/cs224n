# debugging theano.sh
# not actually a bash script.  The fiel extension so we get nice formatting!
# "Debugging Theano is a bit of a black magic" --Jon Gauthier

#activate virtual environment
bash
source deeplearning_env/bin/activate

#RUN WITH GPU:
THEANO_FLAGS=device=gpu python rnn.py

#RUN WITH NVPROF, a profiler that will tell you how much time you spend where: 
CUDA_LAUNCH_BLOCKING=1 THEANO_FLAGS=device=gpu /usr/local/cuda-6.0/bin/nvprof python rnn.py

#send a SIGINT to stop execution and look at profile (note: this takes a bit to actually stop)
^C

# for example of what output might look like, see the bottom of this file.

#alternately: run above command in the background, outputting error to the file rnn_out.log
CUDA_LAUNCH_BLOCKING=1 THEANO_FLAGS=device=gpu /usr/local/cuda-6.0/bin/nvprof python rnn.py > rnn_out.log 2>&1 &
tail -f rnn_out.log 

#???
nvidia-smi
head rnn_out.log 
nvidia-smi

# look at current processes.  Is it all CPU?
top


#----------------------------------------------------------------------------------
#THEANO DEBUGGING
# Say you want to see which parts of a certain theano function are executing on CPU vs. GPU, add in a statement like:

theano.printing.debugprint(f_grad_shared.maker.fgraph.outputs[0])

#Note that his has to be a theano function, not a theano variable
#this will output something like the following.  Note that the following says "Gemm"--such things would say "GPUGemm" 
# were it in GPU!

Elemwise{Composite{(-(i0 / i1))}}[(0, 0)] [@A] ''   
 |Sum{acc_dtype=float64} [@B] ''   
 | |Elemwise{log,no_inplace} [@C] ''   
 |   |Elemwise{Add}[(0, 1)] [@D] ''   
 |     |TensorConstant{(1,) of 1e-08} [@E]
 |     |AdvancedSubtensor [@F] ''   
 |       |SoftmaxWithBias [@G] ''   
 |       | |Dot22 [@H] ''   
 |       | | |Elemwise{Composite{Switch(i0, ((i1 / i2) * i3), ((i4 * i1) / i2))}}[(0, 1)] [@I] ''   
 |       | | | |InplaceDimShuffle{x,x} [@J] ''   
 |       | | | | |<TensorType(float64, scalar)> [@K]
 |       | | | |Sum{axis=[0], acc_dtype=float64} [@L] ''   
 |       | | | | |Elemwise{mul,no_inplace} [@M] ''   
 |       | | | |   |Subtensor{int64:int64:int8} [@N] ''   
 |       | | | |   | |forall_inplace,cpu,lstm__layers}.0 [@O] ''   
 |       | | | |   | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | |Reshape{3} [@Q] ''   
 |       | | | |   | | |   |AdvancedSubtensor1 [@R] ''   
 |       | | | |   | | |   | |Wemb [@S]
 |       | | | |   | | |   | |Flatten{1} [@T] ''   
 |       | | | |   | | |   |   |x [@U]
 |       | | | |   | | |   |MakeVector [@V] ''   
 |       | | | |   | | |     |Shape_i{0} [@W] ''   
 |       | | | |   | | |     | |x [@U]
 |       | | | |   | | |     |Shape_i{1} [@X] ''   
 |       | | | |   | | |     | |x [@U]
 |       | | | |   | | |     |TensorConstant{128} [@Y]
 |       | | | |   | | |InplaceDimShuffle{0,1,x} [@Z] ''   
 |       | | | |   | | | |Subtensor{int64:int64:int8} [@BA] ''   
 |       | | | |   | | |   |mask [@BB]
 |       | | | |   | | |   |ScalarFromTensor [@BC] ''   
 |       | | | |   | | |   | |Elemwise{Composite{Switch(i0, i1, minimum(i2, i3))}} [@BD] ''   
 |       | | | |   | | |   |   |Elemwise{le,no_inplace} [@BE] ''   
 |       | | | |   | | |   |   | |Elemwise{Composite{Switch(i0, Switch(LT((i1 + i2), i3), i3, (i1 + i2)), Switch(LT(i1, i2), i1, i2))}} [@BF] ''   
 |       | | | |   | | |   |   | | |Elemwise{lt,no_inplace} [@BG] ''   
 |       | | | |   | | |   |   | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | |   |   | | | |TensorConstant{0} [@BH]
 |       | | | |   | | |   |   | | |Shape_i{0} [@P] ''   
 |       | | | |   | | |   |   | | |Shape_i{0} [@BI] ''   
 |       | | | |   | | |   |   | | | |mask [@BB]
 |       | | | |   | | |   |   | | |TensorConstant{0} [@BH]
 |       | | | |   | | |   |   | |TensorConstant{0} [@BH]
 |       | | | |   | | |   |   |TensorConstant{0} [@BH]
 |       | | | |   | | |   |   |TensorConstant{0} [@BJ]
 |       | | | |   | | |   |   |Shape_i{0} [@BI] ''   
 |       | | | |   | | |   |ScalarFromTensor [@BK] ''   
 |       | | | |   | | |   | |Elemwise{Composite{Switch(i0, i1, minimum(i2, i3))}}[(0, 2)] [@BL] ''   
 |       | | | |   | | |   |   |Elemwise{le,no_inplace} [@BE] ''   
 |       | | | |   | | |   |   |TensorConstant{0} [@BH]
 |       | | | |   | | |   |   |Elemwise{Composite{Switch(i0, Switch(LT((i1 + i2), i3), i3, (i1 + i2)), Switch(LT(i1, i2), i1, i2))}} [@BF] ''   
 |       | | | |   | | |   |   |Shape_i{0} [@BI] ''   
 |       | | | |   | | |   |Constant{1} [@BM]
 |       | | | |   | | |Elemwise{sub,no_inplace} [@BN] ''   
 |       | | | |   | | | |TensorConstant{(1, 1, 1) of 1.0} [@BO]
 |       | | | |   | | | |InplaceDimShuffle{0,1,x} [@Z] ''   
 |       | | | |   | | |Subtensor{int64:int64:int8} [@BP] ''   
 |       | | | |   | | | |Elemwise{Add}[(0, 0)] [@BQ] ''   
 |       | | | |   | | | | |Reshape{3} [@BR] ''   
 |       | | | |   | | | | | |Dot22 [@BS] ''   
 |       | | | |   | | | | | | |Reshape{2} [@BT] ''   
 |       | | | |   | | | | | | | |InplaceDimShuffle{0,1,2} [@BU] ''   
 |       | | | |   | | | | | | | | |Reshape{3} [@Q] ''   
 |       | | | |   | | | | | | | |MakeVector [@BV] ''   
 |       | | | |   | | | | | | |   |Elemwise{Mul}[(0, 1)] [@BW] ''   
 |       | | | |   | | | | | | |   | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | |   | |Shape_i{1} [@BX] ''   
 |       | | | |   | | | | | | |   |   |Reshape{3} [@Q] ''   
 |       | | | |   | | | | | | |   |TensorConstant{128} [@Y]
 |       | | | |   | | | | | | |Reshape{2} [@BY] ''   
 |       | | | |   | | | | | |   |InplaceDimShuffle{0,1} [@BZ] ''   
 |       | | | |   | | | | | |   | |lstm_W [@CA]
 |       | | | |   | | | | | |   |MakeVector [@CB] ''   
 |       | | | |   | | | | | |     |Shape_i{0} [@CC] ''   
 |       | | | |   | | | | | |     | |lstm_W [@CA]
 |       | | | |   | | | | | |     |Shape_i{1} [@CD] ''   
 |       | | | |   | | | | | |       |lstm_W [@CA]
 |       | | | |   | | | | | |MakeVector [@CE] ''   
 |       | | | |   | | | | |   |Shape_i{0} [@P] ''   
 |       | | | |   | | | | |   |Shape_i{1} [@BX] ''   
 |       | | | |   | | | | |   |Shape_i{1} [@CD] ''   
 |       | | | |   | | | | |InplaceDimShuffle{x,x,0} [@CF] ''   
 |       | | | |   | | | |   |lstm_b [@CG]
 |       | | | |   | | | |ScalarFromTensor [@CH] ''   
 |       | | | |   | | | | |Elemwise{Composite{Switch(i0, i1, minimum(i2, i3))}} [@CI] ''   
 |       | | | |   | | | |   |Elemwise{le,no_inplace} [@CJ] ''   
 |       | | | |   | | | |   | |Elemwise{Composite{Switch(i0, Switch(LT((i1 + i2), i3), i3, (i1 + i2)), Switch(LT(i1, i2), i1, i2))}} [@CK] ''   
 |       | | | |   | | | |   | | |Elemwise{lt,no_inplace} [@BG] ''   
 |       | | | |   | | | |   | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | |   | | |Shape_i{0} [@CL] ''   
 |       | | | |   | | | |   | | | |Reshape{3} [@BR] ''   
 |       | | | |   | | | |   | | |TensorConstant{0} [@BH]
 |       | | | |   | | | |   | |TensorConstant{0} [@BH]
 |       | | | |   | | | |   |TensorConstant{0} [@BH]
 |       | | | |   | | | |   |TensorConstant{0} [@BJ]
 |       | | | |   | | | |   |Shape_i{0} [@CL] ''   
 |       | | | |   | | | |ScalarFromTensor [@CM] ''   
 |       | | | |   | | | | |Elemwise{Composite{Switch(i0, i1, minimum(i2, i3))}}[(0, 2)] [@CN] ''   
 |       | | | |   | | | |   |Elemwise{le,no_inplace} [@CJ] ''   
 |       | | | |   | | | |   |TensorConstant{0} [@BH]
 |       | | | |   | | | |   |Elemwise{Composite{Switch(i0, Switch(LT((i1 + i2), i3), i3, (i1 + i2)), Switch(LT(i1, i2), i1, i2))}} [@CK] ''   
 |       | | | |   | | | |   |Shape_i{0} [@CL] ''   
 |       | | | |   | | | |Constant{1} [@BM]
 |       | | | |   | | |Alloc [@CO] ''   
 |       | | | |   | | | |TensorConstant{0.0} [@CP]
 |       | | | |   | | | |Elemwise{Sub}[(0, 0)] [@CQ] ''   
 |       | | | |   | | | | |Elemwise{Composite{Switch(LT(maximum(i0, i1), i2), Switch(LT((maximum(i0, i1) + i1 + i3), i2), i2, (maximum(i0, i1) + i1 + i3)), Switch(LT(maximum(i0, i1), i4), maximum(i0, i1), i4))}} [@CR] ''   
 |       | | | |   | | | | | |Elemwise{Composite{maximum(maximum(((i0 - Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), Composite{((((i0 - i1) - i2) // i2) + i2)}(Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4), i5, i3), i6, i3), i6, (Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i3), i6), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i6)), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), Composite{((((i0 - i1) - i2) // i2) + i2)}(Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4), i5, i3), i6, i3), i6, (Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i3), i6), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i6))) + i3), i3), maximum(((i0 - i7) + i3), i3))}}[(0, 4)] [@CS] ''   
 |       | | | |   | | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | |Elemwise{ge,no_inplace} [@CT] ''   
 |       | | | |   | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4), i5, Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3))}}[(0, 1)] [@CU] ''   
 |       | | | |   | | | | | | | | |Elemwise{lt,no_inplace} [@CV] ''   
 |       | | | |   | | | | | | | | | |Elemwise{Composite{Switch(i0, i1, maximum(minimum((i2 + i3), i4), i5))}}[(0, 3)] [@CW] ''   
 |       | | | |   | | | | | | | | | | |Elemwise{le,no_inplace} [@CX] ''   
 |       | | | |   | | | | | | | | | | | |Elemwise{sub,no_inplace} [@CY] ''   
 |       | | | |   | | | | | | | | | | | | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@CZ] ''   
 |       | | | |   | | | | | | | | | | | | | |Elemwise{add,no_inplace} [@DA] ''   
 |       | | | |   | | | | | | | | | | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | | | | | | | | | |Elemwise{Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, maximum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4), i6, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, maximum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5))}}[(0, 6)] [@DC] ''   
 |       | | | |   | | | | | | | | | | | | | |   |Elemwise{le,no_inplace} [@DD] ''   
 |       | | | |   | | | | | | | | | | | | | |   | |Elemwise{Composite{Switch(i0, Switch(LT(Composite{((i0 + i1) - i2)}(i1, i2, i3), i4), i4, Composite{((i0 + i1) - i2)}(i1, i2, i3)), Switch(LT(i1, (i2 - i3)), i1, (i2 - i3)))}} [@DE] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | |Elemwise{lt,no_inplace} [@BG] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@DF] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | | |Elemwise{lt,no_inplace} [@BG] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |   | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |   | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4)}} [@DH] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | | | | | | | | |   | | | |Elemwise{add,no_inplace} [@DI] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | | | | | |   | | | | |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |   | | | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@DF] ''   
 |       | | | |   | | | | | | | | | | | | | |   | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |   | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |   |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |   |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4)}} [@DH] ''   
 |       | | | |   | | | | | | | | | | | | | |   |Elemwise{Add}[(0, 1)] [@DK] ''   
 |       | | | |   | | | | | | | | | | | | | |   | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | | | | | |   | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@DF] ''   
 |       | | | |   | | | | | | | | | | | | | |   |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | | | | | | |   |TensorConstant{-1} [@DL]
 |       | | | |   | | | | | | | | | | | | | |   |Elemwise{add,no_inplace} [@DI] ''   
 |       | | | |   | | | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (((i1 + i2) - Switch(GE(i3, i4), i4, i3)) // i0))}(i1, i2, i3, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, Switch(AND(LT(i2, i1), GT(i3, i1)), (i4 - i5), maximum((i4 + i6), i2)))}(i4, i5, (i6 - i7), i7, i2, i8, i9), i5, i8), i5, i10), i8), i5), i5, i3), i5), i11), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (((i1 + i2) - Switch(GE(i3, i4), i4, i3)) // i0))}(i1, i2, i3, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, Switch(AND(LT(i2, i1), GT(i3, i1)), (i4 - i5), maximum((i4 + i6), i2)))}(i4, i5, (i6 - i7), i7, i2, i8, i9), i5, i8), i5, i10), i8), i5), i5, i3), i5), i11)}}[(0, 0)] [@DM] ''   
 |       | | | |   | | | | | | | | | | | |   |Elemwise{add,no_inplace} [@DA] ''   
 |       | | | |   | | | | | | | | | | | |   |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | | | | | | |   |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | | | |   |Elemwise{Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, maximum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4), i6, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, maximum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5))}}[(0, 6)] [@DC] ''   
 |       | | | |   | | | | | | | | | | | |   |Elemwise{le,no_inplace} [@DD] ''   
 |       | | | |   | | | | | | | | | | | |   |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | |   |Elemwise{Add}[(0, 1)] [@DK] ''   
 |       | | | |   | | | | | | | | | | | |   |Elemwise{Composite{Switch(i0, Switch(LT(Composite{((i0 + i1) - i2)}(i1, i2, i3), i4), i4, Composite{((i0 + i1) - i2)}(i1, i2, i3)), Switch(LT(i1, (i2 - i3)), i1, (i2 - i3)))}} [@DE] ''   
 |       | | | |   | | | | | | | | | | | |   |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | | | | |   |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4)}} [@DH] ''   
 |       | | | |   | | | | | | | | | | | |   |TensorConstant{-1} [@DL]
 |       | | | |   | | | | | | | | | | | |   |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@CZ] ''   
 |       | | | |   | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@CZ] ''   
 |       | | | |   | | | | | | | | | | |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | | | |TensorConstant{0} [@BJ]
 |       | | | |   | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | |Elemwise{Composite{Switch(i0, i1, maximum(minimum((i2 + i3), i4), i5))}}[(0, 3)] [@CW] ''   
 |       | | | |   | | | | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | |TensorConstant{-1} [@DL]
 |       | | | |   | | | | | | | |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | | | |   |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | |   |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4), i5, Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3))}}[(0, 1)] [@CU] ''   
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i6, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}}[(0, 1)] [@DO] ''   
 |       | | | |   | | | | | | | |Elemwise{lt,no_inplace} [@DP] ''   
 |       | | | |   | | | | | | | | |Elemwise{Composite{Switch(i0, i1, Switch(AND(LT((i2 + i3), i1), GT(i4, i1)), i5, minimum((i2 + i3), i6)))}}[(0, 3)] [@DQ] ''   
 |       | | | |   | | | | | | | | | |Elemwise{le,no_inplace} [@CX] ''   
 |       | | | |   | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (((i1 + i2) - Switch(GE(i3, i4), i4, i3)) // i0))}(i1, i2, i3, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, Switch(AND(LT(i2, i1), GT(i3, i1)), (i4 - i5), maximum((i4 + i6), i2)))}(i4, i5, (i6 - i7), i7, i2, i8, i9), i5, i8), i5, i10), i8), i5), i5, i3), i5), i11), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (((i1 + i2) - Switch(GE(i3, i4), i4, i3)) // i0))}(i1, i2, i3, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, Switch(AND(LT(i2, i1), GT(i3, i1)), (i4 - i5), maximum((i4 + i6), i2)))}(i4, i5, (i6 - i7), i7, i2, i8, i9), i5, i8), i5, i10), i8), i5), i5, i3), i5), i11)}}[(0, 0)] [@DM] ''   
 |       | | | |   | | | | | | | | | |Elemwise{sub,no_inplace} [@CY] ''   
 |       | | | |   | | | | | | | | | |Elemwise{sub,no_inplace} [@DR] ''   
 |       | | | |   | | | | | | | | | | |TensorConstant{-2} [@DS]
 |       | | | |   | | | | | | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | | | |Elemwise{switch,no_inplace} [@DG] ''   
 |       | | | |   | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | |Elemwise{Composite{Switch(i0, i1, Switch(AND(LT((i2 + i3), i1), GT(i4, i1)), i5, minimum((i2 + i3), i6)))}}[(0, 3)] [@DQ] ''   
 |       | | | |   | | | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | |TensorConstant{-1} [@DL]
 |       | | | |   | | | | | | | |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(LT(i0, i1), i0, i1)}} [@DT] ''   
 |       | | | |   | | | | | |   |TensorConstant{1} [@DB]
 |       | | | |   | | | | | |   |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | |Shape_i{1} [@BX] ''   
 |       | | | |   | | | |TensorConstant{128} [@Y]
 |       | | | |   | | |Alloc [@DU] ''   
 |       | | | |   | | | |TensorConstant{0.0} [@CP]
 |       | | | |   | | | |Elemwise{Sub}[(0, 0)] [@DV] ''   
 |       | | | |   | | | | |Elemwise{Composite{Switch(LT(maximum(i0, i1), i2), Switch(LT((maximum(i0, i1) + i1 + i3), i2), i2, (maximum(i0, i1) + i1 + i3)), Switch(LT(maximum(i0, i1), i4), maximum(i0, i1), i4))}} [@DW] ''   
 |       | | | |   | | | | | |Elemwise{Composite{maximum(maximum(((i0 - Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), Composite{((((i0 - Switch(GE(i1, i2), i2, i1)) - i3) // i3) + i3)}(Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)), Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i6, i7, i0, i3), i4, i5), (i0 + i3), i3), i4, i3), i4, (Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i4)), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), Composite{((((i0 - Switch(GE(i1, i2), i2, i1)) - i3) // i3) + i3)}(Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)), Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i6, i7, i0, i3), i4, i5), (i0 + i3), i3), i4, i3), i4, (Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i1, i2, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i4))) + i3), i3), maximum(((i0 - Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), Composite{((((i0 - Switch(GE(i1, i2), i2, i1)) - i3) // i3) + i3)}(Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)), Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i10, i11, i0, i3), i4, i5), (i0 + i3), i3), i4, i3), i4, (Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i4)), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), Composite{((((i0 - Switch(GE(i1, i2), i2, i1)) - i3) // i3) + i3)}(Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)), Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i10, i11, i0, i3), i4, i5), (i0 + i3), i3), i4, i3), i4, (Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(GE(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5), i6), i7, Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(i0, (i1 + i2 + i3), i1)}(i0, i1, i2, i3), i4, i5))}(i8, i9, i0, i3, i4, i5, (i0 + i3), ((i0 + i3) - i3)) + i3), i4))) + i3), i3))}} [@DX] ''   
 |       | | | |   | | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | |Elemwise{lt,no_inplace} [@DY] ''   
 |       | | | |   | | | | | | | |Elemwise{Composite{Switch(i0, i1, maximum(i2, (i3 - i4)))}}[(0, 4)] [@DZ] ''   
 |       | | | |   | | | | | | | | |Elemwise{le,no_inplace} [@EA] ''   
 |       | | | |   | | | | | | | | | |Elemwise{sub,no_inplace} [@EB] ''   
 |       | | | |   | | | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4), i1), i1, Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4))}}[(0, 2)] [@EC] ''   
 |       | | | |   | | | | | | | | | | | |Elemwise{le,no_inplace} [@ED] ''   
 |       | | | |   | | | | | | | | | | | | |Elemwise{Composite{Switch(i0, Switch(LT((i1 + i2), i3), i3, (i1 + i2)), Switch(LT(i1, i2), i1, i2))}} [@EE] ''   
 |       | | | |   | | | | | | | | | | | | | |Elemwise{lt,no_inplace} [@BG] ''   
 |       | | | |   | | | | | | | | | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | | | | | | | |Elemwise{Composite{Switch(LT((i0 + i1), i2), i2, (i0 + i1))}} [@EF] ''   
 |       | | | |   | | | | | | | | | | | | | | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | | | | | | |Elemwise{sub,no_inplace} [@EG] ''   
 |       | | | |   | | | | | | | | | | | | | | | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@EH] ''   
 |       | | | |   | | | | | | | | | | | | | | | | |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | | | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4)}} [@EI] ''   
 |       | | | |   | | | | | | | | | | | | | | |   |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | | | | | | | | | | | |   |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | | | | | | | | | |   |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | | | | | | | | | | |   |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | | |   |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@EH] ''   
 |       | | | |   | | | | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | | | |Elemwise{Composite{Switch(i0, Switch(LT((i1 + i2), i3), i3, (i1 + i2)), Switch(LT(i1, i2), i1, i2))}} [@EE] ''   
 |       | | | |   | | | | | | | | | | | |Elemwise{Composite{Switch(LT((i0 + i1), i2), i2, (i0 + i1))}} [@EF] ''   
 |       | | | |   | | | | | | | | | | | |Elemwise{sub,no_inplace} [@EG] ''   
 |       | | | |   | | | | | | | | | | | |TensorConstant{-1} [@DL]
 |       | | | |   | | | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1), i4), i1), i5), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1), i4), i1), i5)}}[(0, 3)] [@EJ] ''   
 |       | | | |   | | | | | | | | | |   |Elemwise{le,no_inplace} [@ED] ''   
 |       | | | |   | | | | | | | | | |   |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | | |   |TensorConstant{0} [@BJ]
 |       | | | |   | | | | | | | | | |   |Elemwise{Composite{Switch(LT((i0 + i1), i2), i2, (i0 + i1))}} [@EF] ''   
 |       | | | |   | | | | | | | | | |   |Elemwise{sub,no_inplace} [@EG] ''   
 |       | | | |   | | | | | | | | | |   |Elemwise{Composite{Switch(LT(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4), i1), i1, Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4))}}[(0, 2)] [@EC] ''   
 |       | | | |   | | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4)}} [@EI] ''   
 |       | | | |   | | | | | | | | |Elemwise{Add}[(0, 1)] [@EK] ''   
 |       | | | |   | | | | | | | | | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | | |Elemwise{Composite{Switch(LT(i0, i1), i1, i0)}} [@EH] ''   
 |       | | | |   | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1), i4), i1), i5), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1), i4), i1), i5)}}[(0, 3)] [@EJ] ''   
 |       | | | |   | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(i0, i1, maximum(i2, (i3 - i4)))}}[(0, 4)] [@DZ] ''   
 |       | | | |   | | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | |TensorConstant{-1} [@DL]
 |       | | | |   | | | | | | |Elemwise{lt,no_inplace} [@EL] ''   
 |       | | | |   | | | | | | | |Elemwise{Composite{Switch(i0, i1, Switch(AND(LT((i2 - i3), i1), GT(i4, i1)), i5, maximum((i6 + i7), (i2 - i3))))}}[(0, 2)] [@EM] ''   
 |       | | | |   | | | | | | | | |Elemwise{le,no_inplace} [@EA] ''   
 |       | | | |   | | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | | | |Elemwise{Add}[(0, 1)] [@EK] ''   
 |       | | | |   | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4), i1), i1, Composite{Switch(GE(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{Switch(LT(i0, i1), (i0 + i2), i0)}(Composite{Switch(i0, i1, minimum(i2, i3))}(i0, i1, i2, i3), i1, i4), i1, i5), i4))}}[(0, 2)] [@EC] ''   
 |       | | | |   | | | | | | | | |Elemwise{sub,no_inplace} [@EB] ''   
 |       | | | |   | | | | | | | | |Elemwise{sub,no_inplace} [@DR] ''   
 |       | | | |   | | | | | | | | |TensorConstant{-1} [@DJ]
 |       | | | |   | | | | | | | | |Elemwise{Composite{Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), i2, i0)}(Composite{(i0 - Switch(LT(i1, i2), i2, i1))}(i0, Composite{(i0 + (i1 // i0))}(i1, i2), i3), i3, i2), i3), i4)}} [@EI] ''   
 |       | | | |   | | | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(i0, i1, Switch(AND(LT((i2 - i3), i1), GT(i4, i1)), i5, maximum((i6 + i7), (i2 - i3))))}}[(0, 2)] [@EM] ''   
 |       | | | |   | | | | | | |Elemwise{lt,no_inplace} [@CV] ''   
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(i0, i1, maximum(minimum((i2 + i3), i4), i5))}}[(0, 3)] [@CW] ''   
 |       | | | |   | | | | | | |Elemwise{lt,no_inplace} [@DP] ''   
 |       | | | |   | | | | | | |Elemwise{Composite{Switch(i0, i1, Switch(AND(LT((i2 + i3), i1), GT(i4, i1)), i5, minimum((i2 + i3), i6)))}}[(0, 3)] [@DQ] ''   
 |       | | | |   | | | | | |TensorConstant{1} [@DB]
 |       | | | |   | | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | | | |Shape_i{0} [@P] ''   
 |       | | | |   | | | | | |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | | | | |TensorConstant{0} [@BH]
 |       | | | |   | | | |Shape_i{1} [@BX] ''   
 |       | | | |   | | | |TensorConstant{128} [@Y]
 |       | | | |   | | |lstm_U [@EN]
 |       | | | |   | |ScalarFromTensor [@EO] ''   
 |       | | | |   | | |Elemwise{Composite{(((i0 - i1) - i2) + i3)}}[(0, 0)] [@EP] ''   
 |       | | | |   | |   |Elemwise{Composite{Switch(LT(i0, i1), i0, i1)}} [@DT] ''   
 |       | | | |   | |   |Shape_i{0} [@P] ''   
 |       | | | |   | |   |TensorConstant{1} [@DB]
 |       | | | |   | |   |Elemwise{Composite{maximum(maximum(((i0 - Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), Composite{((((i0 - i1) - i2) // i2) + i2)}(Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4), i5, i3), i6, i3), i6, (Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i3), i6), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i6)), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), Composite{((((i0 - i1) - i2) // i2) + i2)}(Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4), i5, i3), i6, i3), i6, (Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i3), i6), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i6))) + i3), i3), maximum(((i0 - i7) + i3), i3))}}[(0, 4)] [@CS] ''   
 |       | | | |   | |ScalarFromTensor [@EQ] ''   
 |       | | | |   | | |Elemwise{Composite{(((i0 - i1) - i2) + i3)}}[(0, 0)] [@ER] ''   
 |       | | | |   | |   |Elemwise{add,no_inplace} [@DN] ''   
 |       | | | |   | |   |Shape_i{0} [@P] ''   
 |       | | | |   | |   |TensorConstant{1} [@DB]
 |       | | | |   | |   |Elemwise{Composite{maximum(maximum(((i0 - Switch(LT(Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), Composite{((((i0 - i1) - i2) // i2) + i2)}(Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4), i5, i3), i6, i3), i6, (Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i3), i6), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i6)), Composite{Switch(LT(i0, i1), i1, i0)}(Composite{Switch(LT(i0, i1), (i2 - i3), i0)}(Composite{((i0 - (Switch(LT(i1, i2), i2, i1) - i3)) - i3)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), Composite{((((i0 - i1) - i2) // i2) + i2)}(Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4), i5, i3), i6, i3), i6, (Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i3), i6), Composite{Switch(LT(i0, i1), i1, i0)}((Composite{Switch(i0, (i1 - i2), i3)}(i1, i2, i3, i4) + i3), i6))) + i3), i3), maximum(((i0 - i7) + i3), i3))}}[(0, 4)] [@CS] ''   
 |       | | | |   | |Constant{1} [@BM]
 |       | | | |   |InplaceDimShuffle{0,1,x} [@ES] ''   
 |       | | | |     |mask [@BB]
 |       | | | |InplaceDimShuffle{0,x} [@ET] ''   
 |       | | | | |Sum{axis=[0], acc_dtype=float64} [@EU] ''   
 |       | | | |   |mask [@BB]
 |       | | | |Elemwise{Composite{Cast{float64}(LT(i0, i1))}}[(0, 0)] [@EV] ''   
 |       | | | | |mrg_uniform{TensorType(float64, matrix),inplace}.1 [@EW] ''   
 |       | | | | | |<TensorType(int32, matrix)> [@EX]
 |       | | | | | |Elemwise{Cast{int32}} [@EY] ''   
 |       | | | | |   |MakeVector [@EZ] ''   
 |       | | | | |     |Shape_i{1} [@FA] ''   
 |       | | | | |     | |Rebroadcast{0} [@FB] ''   
 |       | | | | |     |   |InplaceDimShuffle{x,0,1} [@FC] ''   
 |       | | | | |     |     |Alloc [@FD] ''   
 |       | | | | |     |       |TensorConstant{0.0} [@CP]
 |       | | | | |     |       |Shape_i{1} [@BX] ''   
 |       | | | | |     |       |TensorConstant{128} [@FE]
 |       | | | | |     |Shape_i{2} [@FF] ''   
 |       | | | | |       |Rebroadcast{0} [@FB] ''   
 |       | | | | |TensorConstant{(1, 1) of 0.5} [@FG]
 |       | | | |TensorConstant{(1, 1) of 0.5} [@FH]
 |       | | |U [@FI]
 |       | |b [@FJ]
 |       |ARange [@FK] ''   
 |       | |TensorConstant{0} [@BH]
 |       | |Shape_i{1} [@X] ''   
 |       | |TensorConstant{1} [@FL]
 |       |y [@FM]
 |Elemwise{Cast{float64}} [@FN] ''   
   |Shape_i{0} [@FO] ''   
     |y [@FM]

Inner graphs of the scan ops:

forall_inplace,cpu,lstm__layers}.0 [@O] ''   
 >Elemwise{Composite{((i0 * scalar_sigmoid(i1) * tanh(i2)) + (i3 * i4))}} [@FP] ''   
 > |<TensorType(float64, col)> [@FQ]
 > |Subtensor{::, int64:int64:} [@FR] ''   
 > | |Gemm{no_inplace} [@FS] ''   
 > | | |<TensorType(float64, matrix)> [@FT]
 > | | |TensorConstant{1.0} [@FU]
 > | | |<TensorType(float64, matrix)> [@FV]
 > | | |lstm_U_copy [@FW]
 > | | |TensorConstant{1.0} [@FU]
 > | |Constant{256} [@FX]
 > | |Constant{384} [@FY]
 > |Elemwise{Composite{((i0 * ((scalar_sigmoid(i1) * i2) + (scalar_sigmoid(i3) * tanh(i4)))) + (i5 * i2))}} [@FZ] ''   
 > | |<TensorType(float64, col)> [@FQ]
 > | |Subtensor{::, int64:int64:} [@GA] ''   
 > | | |Gemm{no_inplace} [@FS] ''   
 > | | |Constant{128} [@GB]
 > | | |Constant{256} [@FX]
 > | |<TensorType(float64, matrix)> [@GC]
 > | |Subtensor{::, int64:int64:} [@GD] ''   
 > | | |Gemm{no_inplace} [@FS] ''   
 > | | |Constant{0} [@GE]
 > | | |Constant{128} [@GB]
 > | |Subtensor{::, int64:int64:} [@GF] ''   
 > | | |Gemm{no_inplace} [@FS] ''   
 > | | |Constant{384} [@FY]
 > | | |Constant{512} [@GG]
 > | |<TensorType(float64, col)> [@GH]
 > |<TensorType(float64, col)> [@GH]
 > |<TensorType(float64, matrix)> [@FV]
 >Elemwise{Composite{((i0 * ((scalar_sigmoid(i1) * i2) + (scalar_sigmoid(i3) * tanh(i4)))) + (i5 * i2))}} [@FZ] '' 






 #--------------------------------------------------
 # example nvv output:
 ==1439== Profiling application: python rnn.py
==1439== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 45.80%  16.145us         1  16.145us  16.145us  16.145us  kernel_reduce_ccontig_node_c8d7bd33dfef61705c2854dd1f0cb7ce_0(unsigned int, float const *, float*)
 36.95%  13.024us         3  4.3410us  1.5680us  9.6640us  [CUDA memcpy HtoD]
 11.53%  4.0640us         1  4.0640us  4.0640us  4.0640us  [CUDA memset]
  5.72%  2.0160us         1  2.0160us  2.0160us  2.0160us  [CUDA memcpy DtoH]

==1439== API calls:
Time(%)      Time     Calls       Avg       Min       Max  Name
 48.20%  248.75ms         4  62.187ms  16.421us  248.66ms  cudaMemcpy
 29.25%  150.97ms         1  150.97ms  150.97ms  150.97ms  cudaThreadExit
 21.67%  111.82ms         7  15.975ms  13.645us  110.35ms  cudaMalloc
  0.34%  1.7538ms         8  219.23us  2.9190us  487.25us  cudaFree
  0.30%  1.5434ms       166  9.2970us     259ns  373.01us  cuDeviceGetAttribute
  0.11%  565.82us         7  80.832us  67.306us  125.41us  cudaGetDeviceProperties
  0.04%  218.51us         2  109.25us  90.954us  127.55us  cuDeviceTotalMem
  0.03%  158.32us         2  79.158us  67.543us  90.774us  cuDeviceGetName
  0.02%  82.490us         1  82.490us  82.490us  82.490us  cudaLaunch
  0.01%  72.084us         6  12.014us  2.5010us  33.936us  cudaThreadSynchronize
  0.01%  54.653us         1  54.653us  54.653us  54.653us  cudaMemset
  0.00%  19.137us         8  2.3920us  1.5100us  5.3220us  cudaEventDestroy
  0.00%  16.347us         8  2.0430us  1.0930us  6.3550us  cudaEventCreateWithFlags
  0.00%  9.7250us         4  2.4310us     723ns  4.5350us  cudaGetDevice
  0.00%  8.3420us         9     926ns     679ns  2.1720us  cudaDeviceGetAttribute
  0.00%  3.5320us         2  1.7660us     572ns  2.9600us  cudaGetDeviceCount
  0.00%  3.3100us         1  3.3100us  3.3100us  3.3100us  cudaSetDevice
  0.00%  3.0290us         1  3.0290us  3.0290us  3.0290us  cudaConfigureCall
  0.00%  2.7250us         3     908ns     458ns  1.5150us  cudaSetupArgument
  0.00%  2.6540us         3     884ns     454ns  1.6450us  cuDeviceGetCount
  0.00%  2.6470us         4     661ns     425ns     816ns  cudaDriverGetVersion
  0.00%  2.3490us         4     587ns     346ns  1.1320us  cudaRuntimeGetVersion
  0.00%  1.6360us         3     545ns     372ns     649ns  cuDeviceGet
  0.00%  1.0850us         1  1.0850us  1.0850us  1.0850us  cuDriverGetVersion
  0.00%  1.0770us         1  1.0770us  1.0770us  1.0770us  cuInit
  0.00%     892ns         1     892ns     892ns     892ns  cudaGetLastError