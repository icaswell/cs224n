#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# File: mlp.py
# @Author: Isaac Caswell/whomever wrote the theano tutorial
# @created: 29 Oct 2015
#
#===============================================================================
# DESCRIPTION:
#
# A multilevel perceptron for text classification.  Based heavily off the theano
# tutorial (http://deeplearning.net/tutorial/mlp.html#mlp)
#
#===============================================================================
# CURRENT STATUS: unimplemented (29 Oct 2015)
#===============================================================================
# USAGE:
# [unimplemented]
# 
#===============================================================================
# THEORETICAL OVERVIEW: (largely copypasted from the tutorial)
#-------------------------------------------------------------------------------
# TRAINING 
# To train an MLP, we learn all parameters of the model, and here we use Stochastic
# Gradient Descent with minibatches. The set of parameters to learn is the set 
#\theta = \{W^{(2)},b^{(2)},W^{(1)},b^{(1)}\}. Obtaining the gradients 
# \partial{\ell}/\partial{\theta} can be achieved through the backpropagation algorithm
# (a special case of the chain-rule of derivation). 
# Thankfully, since Theano performs automatic differentation, we will not need to 
# take manual derivatives!
#===============================================================================
# TODO: 
# -everything


#standard modules
import numpy as np
import time
from collections import Counter
import heapq
import matplotlib.pyplot as plt
import argparse
import timeit
import sys, os

#third party:
import theano
import theano.tensor as T

#our modules
from hidden_layer import HiddenLayer
from logistic_regression import LogisticRegression
import constants
import util


#===============================================================================
# Classes
#===============================================================================

class MLP(object):
    """Multi-Layer Perceptron Class

    A multilayer perceptron is a feedforward artificial neural network model
    that has one layer or more of hidden units and nonlinear activations.
    Intermediate layers usually have as activation function tanh or the
    sigmoid function (defined here by a ``HiddenLayer`` class)  while the
    top layer is a softmax layer (defined here by a ``LogisticRegression``
    class).

    NOTE: The objective function (and hence the L1 and L2 regularization constants)
        are defined by the caller.
    """

    def __init__(self, rng, 
                    # input,
                    n_in,
                    n_hidden,
                    n_out,
                    # datasets,
                    n_layers=1,
                    L1_reg=0.0,
                    L2_reg=0.0001,):
        """Initialize the parameters for the multilayer perceptron

        :type rng: np.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.TensorType
        :param input: symbolic variable that describes the input of the
        architecture (one minibatch)

        :type n_in: int
        :param n_in: number of input units, the dimension of the space in
        which the datapoints lie

        :type n_hidden: int
        :param n_hidden: number of hidden units

        :type n_out: int
        :param n_out: number of output units, the dimension of the space in
        which the labels lie

        :type L1_reg: float
        :param L1_reg: L1-norm's weight when added to the objective (see
        regularization)

        :type L2_reg: float
        :param L2_reg: L2-norm's weight when added to the objective (see
        regularization)
        """

        self.minibatch_index = T.lscalar()  # index to a [mini]batch
        self.input = T.matrix('x')  # the data is presented as rasterized images
        self.y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels


        # Since we are dealing with a one hidden layer MLP, this will translate
        # into a HiddenLayer with a tanh activation function connected to the
        # LogisticRegression layer; the activation function can be replaced by
        # sigmoid or any other nonlinear function
        # TODO: make list of hiden layers, then assert that the dimensions line up

        self.hidden_layers = []

        for li in range(n_layers): 
            # TODO: pass in a different random number generator or not?
            in_dim = n_in if li==0 else n_hidden
            input_i = self.input if li==0 else self.hidden_layers[li-1].output
            hiddenLayer_i = HiddenLayer(
                rng=rng,
                input=input_i,
                n_in=in_dim,
                n_out=n_hidden,
                activation=T.tanh
            )
            self.hidden_layers.append(hiddenLayer_i)


        # self.hiddenLayer = HiddenLayer(
        #     rng=rng,
        #     input=self.input,
        #     n_in=n_in,
        #     n_out=n_hidden,
        #     activation=T.tanh
        # )

        # The logistic regression layer gets as input the hidden units
        # of the hidden layer
        # change to be called softmaxLayer?
        self.logRegressionLayer = LogisticRegression(
            input=self.hidden_layers[-1].output,
            n_in=n_hidden,
            n_out=n_out
        )

        self.initialize_regularizations()

        # negative log likelihood of the MLP is given by the negative
        # log likelihood of the output of the model, computed in the
        # logistic regression layer
        self.negative_log_likelihood = (
            self.logRegressionLayer.negative_log_likelihood
        )
        # same holds for the function computing the number of errors
        self.errors = self.logRegressionLayer.errors

        # the parameters of the model are the parameters of the two layer it is
        # made out of
        # self.params = self.hiddenLayer.params + self.logRegressionLayer.params
        self.params = [param for li in range(n_layers) for param in self.hidden_layers[li].params] + self.logRegressionLayer.params

        # the objective we minimize during training is the negative log likelihood of
        # the model plus the regularization terms (L1 and L2); objective is expressed
        # here symbolically
        self.objective = (
            self.negative_log_likelihood(self.y)
            + L1_reg * self.L1
            + L2_reg * self.L2_sqr
        )



    def initialize_regularizations(self):
        # L1 norm ; one regularization option is to enforce L1 norm to
        # be small
        self.L1 = sum(
                abs(self.hidden_layers[li].W).sum() 
                for li in range(len(self.hidden_layers))) \
            + abs(self.logRegressionLayer.W).sum()


        self.L2_sqr = sum(
                abs(self.hidden_layers[li].W**2).sum() 
                for li in range(len(self.hidden_layers))) \
            +  (self.logRegressionLayer.W ** 2).sum()


    def train(self, 
              datasets,        
              n_epochs=2,
              batch_size=20,
              learning_rate=0.01,
              verbose=False,
              ):
        """
        Demonstrate stochastic gradient descent optimization for a multilayer
        perceptron

        This is demonstrated on MNIST.

        :type learning_rate: float
        :param learning_rate: learning rate used (factor for the stochastic
        gradient

        :type n_epochs: int
        :param n_epochs: maximal number of epochs to run the optimizer

        :type datasets: list
        :param datasets: ...?


        """
        ###############
        # TRAIN MODEL #
        ###############
        print '... training'

        train_set_x, train_set_y = datasets[0]
        valid_set_x, valid_set_y = datasets[1]
        test_set_x, test_set_y = datasets[2]

        # compute number of minibatches for training, validation and testing
        n_train_batches = train_set_x.get_value(borrow=True).shape[0] / batch_size
        n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] / batch_size
        n_test_batches = test_set_x.get_value(borrow=True).shape[0] / batch_size        

        # compiling a Theano function that computes the mistakes that are made
        # by the model on a minibatch
        self.test_model = theano.function(
            inputs=[self.minibatch_index],
            outputs=self.errors(self.y),
            givens={
                self.input: test_set_x[self.minibatch_index * batch_size:(self.minibatch_index + 1) * batch_size],
                self.y: test_set_y[self.minibatch_index * batch_size:(self.minibatch_index + 1) * batch_size]
            }
        )

        self.validate_model = theano.function(
            inputs=[self.minibatch_index],
            outputs=self.errors(self.y),
            givens={
                self.input: valid_set_x[self.minibatch_index * batch_size:(self.minibatch_index + 1) * batch_size],
                self.y: valid_set_y[self.minibatch_index * batch_size:(self.minibatch_index + 1) * batch_size]
            }
        )

        gparams = [T.grad(self.objective, param) for param in classifier.params]

        # specify how to update the parameters of the model as a list of
        # (variable, update expression) pairs
        # below is SGD
        updates = [
            (param, param - learning_rate * gparam)
            for param, gparam in zip(self.params, gparams)
        ]

        # compiling a Theano function `train_model` that returns the self.objective, but
        # in the same time updates the parameter of the model based on the rules
        # defined in `updates`
        self.sgd_update = theano.function(
            inputs=[self.minibatch_index],
            outputs=self.objective,
            updates=updates,
            givens={
                self.input: train_set_x[self.minibatch_index * batch_size: (self.minibatch_index + 1) * batch_size],
                self.y: train_set_y[self.minibatch_index * batch_size: (self.minibatch_index + 1) * batch_size]
            }
        )

        # early-stopping parameters
        patience = 10000  # look as this many examples regardless
        patience_increase = 2  # wait this much longer when a new best is
                               # found
        improvement_threshold = 0.995  # a relative improvement of this much is
                                       # considered significant
        validation_frequency = min(n_train_batches, patience / 2)
                                      # go through this many
                                      # minibatche before checking the network
                                      # on the validation set; in this case we
                                      # check every epoch

        best_validation_loss = np.inf
        best_iter = 0
        test_score = 0.
        start_time = timeit.default_timer()

        epoch = 0
        done_looping = False

        #==============================================
        # for each epoch (and if convergence is unmet):
        #   for each minibatch:
        #       perform the update to weights with self.sgd_update
        #       report the validation error for that batch
        #       
        while (epoch < n_epochs) and (not done_looping):
            epoch = epoch + 1
            for minibatch_index in xrange(n_train_batches):

                minibatch_avg_cost = self.sgd_update(minibatch_index)
                # iteration number
                iter = (epoch - 1) * n_train_batches + minibatch_index

                if (iter + 1) % validation_frequency == 0:
                    # compute zero-one loss on validation set
                    validation_losses = [self.validate_model(i) for i
                                         in xrange(n_valid_batches)]
                    this_validation_loss = np.mean(validation_losses)

                    print(
                        'epoch %i, minibatch %i/%i, validation error %f %%' %
                        (
                            epoch,
                            minibatch_index + 1,
                            n_train_batches,
                            this_validation_loss * 100.
                        )
                    )

                    # if we got the best validation score until now
                    if this_validation_loss < best_validation_loss:
                        #improve patience if loss improvement is good enough
                        if (
                            this_validation_loss < best_validation_loss *
                            improvement_threshold
                        ):
                            patience = max(patience, iter * patience_increase)

                        best_validation_loss = this_validation_loss
                        best_iter = iter

                        # test it on the test set
                        test_losses = [self.test_model(i) for i
                                       in xrange(n_test_batches)]
                        test_score = np.mean(test_losses)

                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1, n_train_batches,
                               test_score * 100.))

                if patience <= iter:
                    done_looping = True
                    break

        end_time = timeit.default_timer()
        print(('Optimization complete. Best validation score of %f %% '
               'obtained at iteration %i, with test performance %f %%') %
              (best_validation_loss * 100., best_iter + 1, test_score * 100.))
        print >> sys.stderr, ('The code for file ' +
                              os.path.split(__file__)[1] +
                              ' ran for %.2fm' % ((end_time - start_time) / 60.))



# def test_mlp(learning_rate=0.01, L1_reg=0.00, L2_reg=0.0001, n_epochs=2,
#              dataset='data/mnist.pkl.gz', batch_size=20, n_hidden=500):


if __name__ == '__main__':
    # test_mlp()
    dataset_fname = 'data/mnist.pkl.gz'
    datasets = util.load_data(dataset_fname)
    print datasets

    print '... building the model'

    rng = np.random.RandomState(1234)

    # construct the MLP class
    classifier = MLP(
        rng=rng,
        # input=x,
        n_in=28 * 28,
        n_hidden=500,
        n_out=10,
        n_layers=2,
        # datasets = datasets,
    )

    classifier.train(datasets, n_epochs=1, verbose=True)

