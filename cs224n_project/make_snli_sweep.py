# Create a script to run a random hyperparameter search.

import copy
import random
import numpy as np
import util
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--id', dest = 'ID', default='', type=str)
parser.add_argument('--n', dest = 'N_SWEEP_RUNS', default=4, type=int)

ARGS =  parser.parse_args()
ID_COMMAND = "--id %s "%ARGS.ID if ARGS.ID else ""

random.seed(419)

LIN = "LIN"
EXP = "EXP"

# Instructions: Configure the variables in this block, then run
# the following on a machine with qsub access:
# python make_sweep.py > my_sweep.sh
# bash my_sweep.sh

# - #

FIXED_PARAMETERS = {
    "adv": 1,
    "data":  "imdb",
    "epochs": 500,
    "encoder": "lstm",
    "wdim": 300,
    "maxlen":100,
    "wemb_init":"word2vec",#"random",
    "lrate": 0.001,
}

# LSTM weight (W, U) initialization range: normal with std-dev [0.001, 0.5]

# Tunable parameters.
# Note: there are three options for the keys:
# 1. a 3-tuple of 
#   (EXP or LIN, lower_bound, upper_bound)
#   (EXP or LIN, lower_bound, upper_bound, callable that generates a parameter value)
#   [list of possible values]

SWEEP_PARAMETERS = {
    #"lrate":      (EXP, 0.00001, 0.1), #learning rate
    "batch_size":   (LIN, 8, 256),  
    "optimizer":      ["adadelta", "rmsprop"],   
    "weight-init": (EXP, .001, 0.5, lambda r: "ortho_%s"%r), #how to initialize the weight vectors
    "hdim": (LIN, 64, 512), #hidden dimension
    "reg":   (EXP, 10e-8, 10e-2), # L2 regularization for U
    "eps": (EXP, .00001, 2.0), #adversarial epsilon
    "alpha": (LIN, 0.0, 1.0), #adversarial alpha
    "clip": (LIN, 1.0, 10.0), #clip gradients that have absolute values greater than this
}

sweep_name = "sweep_" + \
    FIXED_PARAMETERS["data"] + "_adv=%s"%FIXED_PARAMETERS["adv"] + "_%s"%util.time_string()

# gpus = [str(i) for i in range(6)]
# n_jobs_per_gpu = 4
# sweep_runs = n_jobs_per_gpu * len(gpus)
sweep_runs = ARGS.N_SWEEP_RUNS
# - #
print "# NAME: " + sweep_name
print "# NUM RUNS: " + str(sweep_runs)
print "# SWEEP PARAMETERS: " + str(SWEEP_PARAMETERS)
print "# FIXED_PARAMETERS: " + str(FIXED_PARAMETERS)
print

for run_id in range(sweep_runs):
    # gpu_id = gpus[run_id%len(gpus)]
    params = {}
    params.update(FIXED_PARAMETERS)
    for param in SWEEP_PARAMETERS:
        config = SWEEP_PARAMETERS[param]

        sample = None
        if isinstance(config, list):
            sample = random.choice(config)
        else:
            r = random.uniform(0, 1)
            t = config[0]
            mn = config[1]
            mx = config[2]

            if t == EXP:
                lmn = np.log(mn)
                lmx = np.log(mx)
                sample = np.exp(lmn + (lmx - lmn) * r)
            else:
                sample = mn + (mx - mn) * r

            if isinstance(mn, int):
                sample = int(round(sample, 0))


            if len(config) == 4:
                #this is the case in which the last element is a function that returns the param value
                assert hasattr(config[3], '__call__')
                sample = config[3](r)

        params[param] = sample

    name = sweep_name + "_" + str(run_id)
    flags = ""
    for param in params:
        value = params[param]
        val_str = ""
        flags += " --" + param + " " + str(value)
        if param not in FIXED_PARAMETERS:
            if isinstance(value, int):
                val_disp = str(value)
            if isinstance(value, str):
                val_disp = value               
            else:
                val_disp = "%.2g" % value
            name += "-" + param + val_disp
    # flags += " --experiment_name " + name
    # print "export REMBED_FLAGS=\"" + flags + "\"; qsub -v REMBED_FLAGS train_adversarial_lstm.sh -q " + queue
    # print "qsub train_adversarial_lstm.sh " + flags
    # the --redirect flag tells the program to redicet all its stdout and stdin to a file of its own naming
    print "screen -dm bash train_adversarial_lstm.sh --redirect " + ID_COMMAND + flags
    # print "THEANO_FLAGS=device=gpu{gid},floatX=float32 nohup  python rnn.py --id gpu_{gid} --redirect ".format(gid=gpu_id) + flags + " &"
    # nohup python deleteme.py >& log  & head log  
    print
