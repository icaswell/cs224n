# rnn_util.py

# from collections import OrderedDict
# import cPickle as pkl
# import sys
# import time
# import util

import numpy
import theano
from theano import config
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams


def ortho_weight(ndim):
    W = numpy.random.randn(ndim, ndim)
    u, s, v = numpy.linalg.svd(W)
    return u.astype(config.floatX)


def _p(pp, name):
    #add a prefix to a name
    return '%s_%s' % (pp, name)

def numpy_floatX(data):
    return numpy.asarray(data, dtype=config.floatX)


def _slice(_x, n, dim):
    if _x.ndim == 3:
        return _x[:, :, n * dim:(n + 1) * dim]
    return _x[:, n * dim:(n + 1) * dim]
        
#=======================================================================    

def param_init_lstm(options, params, prefix='lstm'):
    """
    Init the LSTM parameter:

    :see: init_params
    """
    W = numpy.concatenate([ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj'])], axis=1)
    params[_p(prefix, 'W')] = W
    U = numpy.concatenate([ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj'])], axis=1)
    params[_p(prefix, 'U')] = U
    b = numpy.zeros((4 * options['dim_proj'],))
    params[_p(prefix, 'b')] = b.astype(config.floatX)

    return params


def lstm_layer(tparams, state_below, options, prefix='lstm', mask=None):
    """
    ------------------------------------------------------------------
    state_below: the word embedding matrix (in the vanilla lstm case)

    :return: a theano symbolic variable representing the lstm layer on 
        top of whatever came below it

    variables internal to this function:
        nsteps: the number of timesteps.
            TODO: how is this the same for all input??
    """
    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1

    assert mask is not None


    def _step(m_, x_, h_, c_):
        """
        ----------------------------------------------------------------------
        For some timestep t (_step is agnostic to the index of the timestep):

        m_ == mask[t, :]  mask for THIS timestep.
        x_ == state_below[t, : (,:)] input from layer below for THIS timestep
        	NOTE: the transformation by the matrix W has already be performed.
        h_ == the activation from the PRIOR TIMESTEP (TODO: unknown initialization)
        c_ == c from the PRIOR TIMESTEP (dunno what this is, prolly some lstm thing)
        """
        preact = tensor.dot(h_, tparams[_p(prefix, 'U')])
        preact += x_

        i = tensor.nnet.sigmoid(_slice(preact, 0, options['dim_proj']))
        f = tensor.nnet.sigmoid(_slice(preact, 1, options['dim_proj']))
        o = tensor.nnet.sigmoid(_slice(preact, 2, options['dim_proj']))
        c = tensor.tanh(_slice(preact, 3, options['dim_proj']))

        c = f * c_ + i * c
        c = m_[:, None] * c + (1. - m_)[:, None] * c_

        h = o * tensor.tanh(c)
        h = m_[:, None] * h + (1. - m_)[:, None] * h_

        return h, c

    state_below = (tensor.dot(state_below, tparams[_p(prefix, 'W')]) +
                   tparams[_p(prefix, 'b')])

    dim_proj = options['dim_proj']
    rval, updates = theano.scan(_step,
                                sequences=[mask, state_below],
                                outputs_info=[tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj),
                                              tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj)],
                                name=_p(prefix, '_layers'),
                                n_steps=nsteps)

    # TODO: looks like we're returning only the first hidden activation in the layer--
    # how does this make sense?  
    return rval[0]    







def param_init_rnn_vanilla(options, params, prefix='rnn_vanilla'):
    """
    Init the rnn_vanilla parameter:

    :see: init_params
    """
    W = numpy.concatenate([ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj'])], axis=1)
    params[_p(prefix, 'W')] = W
    U = numpy.concatenate([ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj']),
                           ortho_weight(options['dim_proj'])], axis=1)
    params[_p(prefix, 'U')] = U
    b = numpy.zeros((4 * options['dim_proj'],))
    params[_p(prefix, 'b')] = b.astype(config.floatX)

    return params



def rnn_vanilla_layer(tparams, state_below, options, prefix='rnn_vanilla', mask=None):
    """
    ------------------------------------------------------------------
    state_below: the word embedding matrix (in the vanilla rnn_vanilla case)

    :return: a theano symbolic variable representing the rnn_vanilla layer on 
        top of whatever came below it

    variables internal to this function:
        nsteps: the number of timesteps.
            TODO: how is this the same for all input??
    """
    nsteps = state_below.shape[0]
    if state_below.ndim == 3:
        n_samples = state_below.shape[1]
    else:
        n_samples = 1

    assert mask is not None


    def _step(m_, x_, h_):
        """
        ----------------------------------------------------------------------
        For some timestep t (_step is agnostic to the index of the timestep):

        m_ == mask[t, :]  mask for THIS timestep.
        x_ == state_below[t, : (,:)] input from layer below for THIS timestep
        	NOTE: the transformation by the matrix W has already be performed.
        h_ == the activation from the PRIOR TIMESTEP (TODO: unknown initialization)
        c_ == c from the PRIOR TIMESTEP (dunno what this is, prolly some lstm thing)
        """
        preact_from_prev_state = tensor.dot(h_, tparams[_p(prefix, 'U')])
        preact_from_cur_word = x_#tensor.dot(x_, tparams[_p(prefix, 'W')])        
        preact =  preact_from_prev_state + preact_from_cur_word

        h = tensor.tanh(_slice(preact, 3, options['dim_proj']))
        # h = tensor.tanh(preact)

        #apply dropout
        h = m_[:, None] * h + (1. - m_)[:, None] * h_

        return h

    state_below = (tensor.dot(state_below, tparams[_p(prefix, 'W')]) +
                   tparams[_p(prefix, 'b')])

    dim_proj = options['dim_proj']
    rval, updates = theano.scan(_step,
                                sequences=[mask, state_below],
                                outputs_info=[tensor.alloc(numpy_floatX(0.),
                                                           n_samples,
                                                           dim_proj),
                                              # tensor.alloc(numpy_floatX(0.),
                                              #              n_samples,
                                              #              dim_proj)
                                              ],
                                name=_p(prefix, '_layers'),
                                n_steps=nsteps)

    return rval[0]