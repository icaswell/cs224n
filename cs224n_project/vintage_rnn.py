#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# File: rnn.py
# @Author: Isaac Caswell/whomever wrote the theano tutorial
# @created: 1 Nov 2015
#
#===============================================================================
# DESCRIPTION:
#
# trains and tests an rnn.  Can be given ots of options, like whether to use LSTM
#
#===============================================================================
# CURRENT STATUS: works (1 Nov 2015)
#===============================================================================
# USAGE:
#  python rnn.py
#===============================================================================
# TODO: 
# -document, conjugate verbs in comments
# document more!!
# maybe make into a class/decompose
# make models save to a folder called 'models'
# 
# Why is the dim_proj both the word embedding and the hidden layer dimension?


from collections import OrderedDict
import cPickle as pkl
import sys
import time
import util
import argparse

import numpy
import theano
from theano import config
import theano.tensor as tensor
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

from dataloader import load_data, prepare_data
from rnn_util_vintage import *

#================================================================================
def get_args():
    """
    parses command line args"""
    parser = argparse.ArgumentParser() 

    parser.add_argument('--adv', dest = 'ADVERSARIAL', action = "store_true", default = False, help="by default, ADVERSARIAL is set to False.  Running with the --adv flag sets it to true.")#ADVERSARIAL = False
    parser.add_argument('-data', dest = 'DATANAME', type=str, default='imdb', help = "'imdb' or 'toy_corpus'")## DATASET = "./data/toy_corpus.pkl"

    parser.add_argument('-epochs', dest = 'MAX_EPOCHS', type=int, default=1000)#MAX_EPOCHS = 2
    parser.add_argument('-alpha', dest = 'ADVERSARIAL_ALPHA', type=float, default=0.25)#ADVERSARIAL_ALPHA = 0.9
    parser.add_argument('-eps', dest = 'ADVERSARIAL_EPSILON', type=float, default=0.5)#ADVERSARIAL_EPSILON = .07
    parser.add_argument('-encoder', dest = 'ENCODER', type=str, default='lstm', help="lst or rnn_vanilla")#ENCODER = 'lstm' if 0 else 'rnn_vanilla'    

    return parser.parse_args()
#================================================================================

# DATAPATH = "data/imdb.pkl"

# Set the random number generators' seeds for consistency
SEED = 123
numpy.random.seed(SEED)
REAL_STDOUT = sys.stdout

# ff: Feed Forward (normal neural net), only useful to put after lstm
#     before the classifier.
layers = {'lstm': (param_init_lstm, lstm_layer),
          'rnn_vanilla': (param_init_rnn_vanilla, rnn_vanilla_layer),
            }


def get_minibatches_idx(n, minibatch_size, shuffle=False):
    """
    Used to shuffle the dataset at each iteration.
    -----------------------------------------------------
    :param int n: the number of examples in question
    returns a list fo tuples of the form
        (minibatch_idx, [list of indexes of examples])
    """

    idx_list = numpy.arange(n, dtype="int32")

    if shuffle:
        numpy.random.shuffle(idx_list)

    minibatches = []
    minibatch_start = 0
    for i in range(n // minibatch_size):
        minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
        minibatch_start += minibatch_size

    if (minibatch_start != n):
        # Make a minibatch out of what is left
        minibatches.append(idx_list[minibatch_start:])

    return zip(range(len(minibatches)), minibatches)


def zipp(params, tparams):
    """
    When we reload the model. Needed for the GPU stuff.
    """
    for kk, vv in params.iteritems():
        tparams[kk].set_value(vv)


def unzip(zipped):
    """
    When we pickle the model. Needed for the GPU stuff.
    """
    new_params = OrderedDict()
    for kk, vv in zipped.iteritems():
        new_params[kk] = vv.get_value()
    return new_params


def dropout_layer(state_before, use_noise, trng):
    proj = tensor.switch(use_noise,
                         (state_before *
                          trng.binomial(state_before.shape,
                                        p=0.5, n=1,
                                        dtype=state_before.dtype)),
                         state_before * 0.5)
    return proj


def init_params(options):
    """
    Global (not LSTM) parameter. For the embeding and the classifier.
    ----------------------------------------------------------------
    Randomly initializes:
        -the word embedding matrix  
            -shape = (n_words,dim_proj)
        -the weight matrix U
            -shape = (dim_proj, y_dim) 
        -the intercept b
            -shape = (y_dim,)                    
    """
    params = OrderedDict()
    # embedding
    randn = numpy.random.rand(options['n_words'],
                              options['dim_proj'])
    params['Wemb'] = (0.01 * randn).astype(config.floatX)
    params = get_layer(options['encoder'])[0](options,
                                              params,
                                              prefix=options['encoder'])
    # classifier
    params['U'] = 0.01 * numpy.random.randn(options['dim_proj'],
                                            options['ydim']).astype(config.floatX)
    params['b'] = numpy.zeros((options['ydim'],)).astype(config.floatX)

    return params


def load_params(path, params):
    pp = numpy.load(path)
    for kk, vv in params.iteritems():
        if kk not in pp:
            raise Warning('%s is not in the archive' % kk)
        params[kk] = pp[kk]

    return params


def init_tparams(params):
    tparams = OrderedDict()
    for kk, pp in params.iteritems():
        tparams[kk] = theano.shared(params[kk], name=kk)
    return tparams


def get_layer(name):
    # fns = e.g. (param_init_lstm, lstm_layer)
    fns = layers[name]
    return fns





def sgd(lr, tparams, grads, x, mask, y, cost):
    """ Stochastic Gradient Descent

    :note: A more complicated version of sgd than needed.  This is
        done like that for adadelta and rmsprop.

    """
    # New set of shared variable that will contain the gradient
    # for a mini-batch.
    gshared = [theano.shared(p.get_value() * 0., name='%s_grad' % k)
               for k, p in tparams.iteritems()]
    gsup = [(gs, g) for gs, g in zip(gshared, grads)]

    # Function that computes gradients for a mini-batch, but do not
    # updates the weights.
    f_grad_shared = theano.function([x, mask, y], cost, updates=gsup,
                                    name='sgd_f_grad_shared')

    pup = [(p, p - lr * g) for p, g in zip(tparams.values(), gshared)]

    # Function that updates the weights from the previously computed
    # gradient.
    f_update = theano.function([lr], [], updates=pup,
                               name='sgd_f_update')

    return f_grad_shared, f_update


def adadelta(lr, tparams, grads, x, mask, y, cost):
    """
    An adaptive learning rate optimizer

    Parameters
    ----------
    lr : Theano SharedVariable
        Initial learning rate
    tpramas: Theano SharedVariable
        Model parameters
    grads: Theano variable
        Gradients of cost w.r.t to parameres
    x: Theano variable
        Model inputs
    mask: Theano variable
        Sequence mask
    y: Theano variable
        Targets
    cost: Theano variable
        Objective fucntion to minimize

    Notes
    -----
    For more information, see [ADADELTA]_.

    .. [ADADELTA] Matthew D. Zeiler, *ADADELTA: An Adaptive Learning
       Rate Method*, arXiv:1212.5701.
    """

    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                  name='%s_grad' % k)
                    for k, p in tparams.iteritems()]
    running_up2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                 name='%s_rup2' % k)
                   for k, p in tparams.iteritems()]
    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                    name='%s_rgrad2' % k)
                      for k, p in tparams.iteritems()]

    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
    rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
             for rg2, g in zip(running_grads2, grads)]

    f_grad_shared = theano.function([x, mask, y], cost, updates=zgup + rg2up,
                                    name='adadelta_f_grad_shared')

    updir = [-tensor.sqrt(ru2 + 1e-6) / tensor.sqrt(rg2 + 1e-6) * zg
             for zg, ru2, rg2 in zip(zipped_grads,
                                     running_up2,
                                     running_grads2)]
    ru2up = [(ru2, 0.95 * ru2 + 0.05 * (ud ** 2))
             for ru2, ud in zip(running_up2, updir)]
    param_up = [(p, p + ud) for p, ud in zip(tparams.values(), updir)]

    f_update = theano.function([lr], [], updates=ru2up + param_up,
                               on_unused_input='ignore',
                               name='adadelta_f_update')

    return f_grad_shared, f_update


def rmsprop(lr, tparams, grads, x, mask, y, cost):
    """
    A variant of  SGD that scales the step size by running average of the
    recent step norms.

    Parameters
    ----------
    lr : Theano SharedVariable
        Initial learning rate
    tpramas: Theano SharedVariable
        Model parameters
    grads: Theano variable
        Gradients of cost w.r.t to parameres
    x: Theano variable
        Model inputs
    mask: Theano variable
        Sequence mask
    y: Theano variable
        Targets
    cost: Theano variable
        Objective fucntion to minimize

    Notes
    -----
    For more information, see [Hint2014]_.

    .. [Hint2014] Geoff Hinton, *Neural Networks for Machine Learning*,
       lecture 6a,
       http://cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf
    """

    zipped_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                  name='%s_grad' % k)
                    for k, p in tparams.iteritems()]
    running_grads = [theano.shared(p.get_value() * numpy_floatX(0.),
                                   name='%s_rgrad' % k)
                     for k, p in tparams.iteritems()]
    running_grads2 = [theano.shared(p.get_value() * numpy_floatX(0.),
                                    name='%s_rgrad2' % k)
                      for k, p in tparams.iteritems()]

    zgup = [(zg, g) for zg, g in zip(zipped_grads, grads)]
    rgup = [(rg, 0.95 * rg + 0.05 * g) for rg, g in zip(running_grads, grads)]
    rg2up = [(rg2, 0.95 * rg2 + 0.05 * (g ** 2))
             for rg2, g in zip(running_grads2, grads)]

    f_grad_shared = theano.function([x, mask, y], cost,
                                    updates=zgup + rgup + rg2up,
                                    name='rmsprop_f_grad_shared')

    updir = [theano.shared(p.get_value() * numpy_floatX(0.),
                           name='%s_updir' % k)
             for k, p in tparams.iteritems()]
    updir_new = [(ud, 0.9 * ud - 1e-4 * zg / tensor.sqrt(rg2 - rg ** 2 + 1e-4))
                 for ud, zg, rg, rg2 in zip(updir, zipped_grads, running_grads,
                                            running_grads2)]
    param_up = [(p, p + udn[1])
                for p, udn in zip(tparams.values(), updir_new)]
    f_update = theano.function([lr], [], updates=updir_new + param_up,
                               on_unused_input='ignore',
                               name='rmsprop_f_update')

    return f_grad_shared, f_update


def build_model(tparams, options,
                adversarial,
                adv_epsilon, 
                adv_alpha):
    """
    #-------------------------------------------------------------
    creates the symbolic variables used by the model.  These include:
        -x: the input matrix, where each word is represented as an index
            -shape (n_timesteps, n_samples)
        -y:
            shape: 
        -emb: analogous to x, only using the full embedding of the word.
            -shape (n_timesteps, n_samples, dim_proj)
        -proj: an lstm_layer instance
    """
    trng = RandomStreams(SEED)

    # Used for dropout.
    use_noise = theano.shared(numpy_floatX(0.))

    x = tensor.matrix('x', dtype='int64')
    mask = tensor.matrix('mask', dtype=config.floatX)
    y = tensor.vector('y', dtype='int64')

    # note that some sequences are padded 
    n_timesteps = x.shape[0]
    n_samples = x.shape[1]

    emb = tparams['Wemb'][x.flatten()].reshape([n_timesteps,
                                                n_samples,
                                                options['dim_proj']])


    # get_layer returns (param_init_lstm, lstm_layer)
    # TODO: why does this not crash when options['encoder'] is not equal to 'lstm'?
    proj = get_layer(options['encoder'])[1](tparams, emb, options,
                                            prefix=options['encoder'],
                                            mask=mask)
    if options['encoder'] == 'lstm':
        proj = (proj * mask[:, :, None]).sum(axis=0)
        proj = proj / mask.sum(axis=0)[:, None]
    if options['use_dropout']:
        proj = dropout_layer(proj, use_noise, trng)

    pred = tensor.nnet.softmax(tensor.dot(proj, tparams['U']) + tparams['b'])

    f_pred_prob = theano.function([x, mask], pred, name='f_pred_prob')
    f_pred = theano.function([x, mask], pred.argmax(axis=1), name='f_pred')

    off = 1e-8
    if pred.dtype == 'float16':
        off = 1e-6

    # the objective function: 
    cost = -tensor.log(pred[tensor.arange(n_samples), y] + off).mean()

    if adversarial:  # done by Isaac
        # adv_x = tensor.matrix('adv_x', dtype='int64')
        # adv_mask = tensor.matrix('adv_mask', dtype=config.floatX)
        leaf_grads = tensor.grad(cost, wrt=emb)

        # treat this as a constant. !!!!!
        # e.g. stop_gradient ("something like this")
        # Victor Zhong
        anti_example = tensor.sgn(leaf_grads)
        adv_example = emb + adv_epsilon*anti_example
        adv_example = theano.gradient.disconnected_grad(adv_example)

        adv_proj = get_layer(options['encoder'])[1](tparams, adv_example, options,
                                            prefix=options['encoder'],
                                            mask=mask)
        if options['encoder'] == 'lstm':
            adv_proj = (adv_proj * mask[:, :, None]).sum(axis=0)
            adv_proj = adv_proj / mask.sum(axis=0)[:, None]
        if options['use_dropout']:
            adv_proj = dropout_layer(adv_proj, use_noise, trng)
        # theano.printing.debugprint(adv_proj)
        # adv_pred = tensor.nnet.softmax(tensor.dot(proj, tparams['U']) + tparams['b'])        
        adv_pred = tensor.nnet.softmax(tensor.dot(adv_proj, tparams['U']) + tparams['b'])
        # adv_f_pred_prob = theano.function([x, mask], pred, name='adv_f_pred_prob')
        # adv_f_pred_prob = theano.function([x, mask], adv_pred, name='adv_f_pred_prob')

        # adv_f_pred = theano.function([x, mask], adv_pred.argmax(axis=1), name='adv_f_pred')
        adv_cost = -tensor.log(adv_pred[tensor.arange(n_samples), y] + off).mean()

        cost = adv_alpha*cost + (1-adv_alpha)*adv_cost
    # theano.printing.pydotprint(cost, outfile="output/lstm_cost_viz.png", var_with_name_simple=True)
    return use_noise, x, mask, y, f_pred_prob, f_pred, cost


def pred_probs(f_pred_prob, prepare_data, data, iterator, verbose=False):
    """ If you want to use a trained model, this is useful to compute
    the probabilities of new examples.
    """
    n_samples = len(data[0])
    probs = numpy.zeros((n_samples, 2)).astype(config.floatX)

    n_done = 0

    for _, valid_index in iterator:
        x, mask, y = prepare_data([data[0][t] for t in valid_index],
                                  numpy.array(data[1])[valid_index],
                                  maxlen=None)
        pred_probs = f_pred_prob(x, mask)
        probs[valid_index, :] = pred_probs

        n_done += len(valid_index)
        if verbose:
            print '%d/%d samples classified' % (n_done, n_samples)

    return probs


def pred_error(f_pred, prepare_data, data, iterator, verbose=False):
    """
    Just compute the error
    f_pred: Theano fct computing the prediction
    prepare_data: usual prepare_data for that dataset.
    """
    valid_err = 0
    for _, valid_index in iterator:
        x, mask, y = prepare_data([data[0][t] for t in valid_index],
                                  numpy.array(data[1])[valid_index],
                                  maxlen=None)
        preds = f_pred(x, mask)
        targets = numpy.array(data[1])[valid_index]
        valid_err += (preds == targets).sum()
    valid_err = 1. - numpy_floatX(valid_err) / len(data[0])

    return valid_err


def train_lstm(
    dim_proj=128,  # word embeding dimension and LSTM number of hidden units.
    patience=10,  # Number of epoch to wait before early stop if no progress
    max_epochs=5000,  # The maximum number of epoch to run
    dispFreq=10,  # Display to stdout the training progress every N updates
    decay_c=0.,  # Weight decay for the classifier applied to the U weights.
    lrate=0.0001,  # Learning rate for sgd (not used for adadelta and rmsprop)
    n_words=10000,  # Vocabulary size
    optimizer=adadelta,  # sgd, adadelta and rmsprop available, sgd very hard to use, not recommanded (probably need momentum and decaying learning rate).
    encoder='lstm',  # TODO: can be removed must be lstm.
    saveto='saved_models/lstm_model.npz',  # The best model will be saved there
    validFreq=370,  # Compute the validation error after this number of update.
    saveFreq=1110,  # Save the parameters after every saveFreq updates
    maxlen=100,  # Sequence longer then this get ignored
    batch_size=16,  # The batch size during training.
    valid_batch_size=64,  # The batch size used for validation/test set.
    dataset="data/imdb.pkl",

    # Parameter for extra option
    noise_std=0.,
    use_dropout=True,  # if False slightly faster, but worst test error
                       # This frequently need a bigger model.
    reload_model=None,  # Path to a saved model we want to start from.
    test_size=-1,  # If >0, we keep only this number of test example.
    adv_epsilon = None,
    adv_alpha = None,
    adversarial = None,
):

    # Model options
    model_options = locals().copy()
    print "model options", model_options

    # load_data, prepare_data = get_dataset(dataset)

    print 'Loading data'
    train, valid, test = load_data(n_words=n_words, valid_portion=0.05,
                                   maxlen=maxlen, path=dataset)
    if test_size > 0:
        # The test set is sorted by size, but we want to keep random
        # size example.  So we must select a random selection of the
        # examples.
        idx = numpy.arange(len(test[0]))
        numpy.random.shuffle(idx)
        idx = idx[:test_size]
        test = ([test[0][n] for n in idx], [test[1][n] for n in idx])

    ydim = numpy.max(train[1]) + 1

    model_options['ydim'] = ydim

    print 'Building model'

    # initialize the word embedding matrix and the parameters of the model (U and b) randomly
    # params is a dict mapping name (string) -> numpy ndarray
    params = init_params(model_options)

    if reload_model:
        load_params('lstm_model.npz', params)

    # This create Theano Shared Variable from the parameters.
    # Dict name (string) -> Theano Tensor Shared Variable
    # params and tparams have different copy of the weights.
    #-------------------------------------------------------------
    # Isaac presumes that having two sets of params has something to 
    # do with efficiency?
    tparams = init_tparams(params)

    # use_noise is for dropout
    (use_noise, x, mask,
     y, f_pred_prob, f_pred, cost) = build_model(tparams, model_options, adv_epsilon=adv_epsilon, adv_alpha=adv_alpha, adversarial = adversarial)

    if decay_c > 0.:
        decay_c = theano.shared(numpy_floatX(decay_c), name='decay_c')
        weight_decay = 0.
        weight_decay += (tparams['U'] ** 2).sum()
        weight_decay *= decay_c
        cost += weight_decay

    f_cost = theano.function([x, mask, y], cost, name='f_cost')

    grads = tensor.grad(cost, wrt=tparams.values())
    f_grad = theano.function([x, mask, y], grads, name='f_grad')

    lr = tensor.scalar(name='lr')
    f_grad_shared, f_update = optimizer(lr, tparams, grads,
                                        x, mask, y, cost)

    print 'Optimization'

    kf_valid = get_minibatches_idx(len(valid[0]), valid_batch_size)
    kf_test = get_minibatches_idx(len(test[0]), valid_batch_size)

    print "%d train examples" % len(train[0])
    print "%d valid examples" % len(valid[0])
    print "%d test examples" % len(test[0])

    history_errs = []
    best_p = None
    bad_count = 0

    if validFreq == -1:
        validFreq = len(train[0]) / batch_size
    if saveFreq == -1:
        saveFreq = len(train[0]) / batch_size

    uidx = 0  # the number of update done
    estop = False  # early stop
    start_time = time.time()
    try:
        for epoch in xrange(max_epochs):
            n_samples = 0

            # Get new shuffled index for the training set.
            minibatches = get_minibatches_idx(len(train[0]), batch_size, shuffle=True)

            for _, train_index_list in minibatches:
                uidx += 1
                use_noise.set_value(1.)

                # Select the random examples for this minibatch
                y = [train[1][t] for t in train_index_list]
                x = [train[0][t]for t in train_index_list]

                # Get the data in numpy.ndarray format
                # This swap the axis!
                # Return something of shape (minibatch maxlen, n samples)
                x, mask, y = prepare_data(x, y)
                n_samples += x.shape[1]

                cost = f_grad_shared(x, mask, y)
                f_update(lrate)

                if numpy.isnan(cost) or numpy.isinf(cost):
                    print 'NaN detected'
                    return 1., 1., 1.

                if numpy.mod(uidx, dispFreq) == 0:
                    print 'Epoch ', epoch, 'Update ', uidx, 'Cost ', cost
                    REAL_STDOUT.write('Epoch '+ str(epoch) + ' Update ' + str(uidx) +' Cost ' + str(cost) + "\n")


                if saveto and numpy.mod(uidx, saveFreq) == 0:
                    print 'Saving...',

                    if best_p is not None:
                        params = best_p
                    else:
                        params = unzip(tparams)
                    numpy.savez(saveto, history_errs=history_errs, **params)
                    pkl.dump(model_options, open('%s.pkl' % saveto, 'wb'), -1)
                    print 'Done'

                if numpy.mod(uidx, validFreq) == 0:
                    use_noise.set_value(0.)
                    train_err = pred_error(f_pred, prepare_data, train, minibatches)
                    valid_err = pred_error(f_pred, prepare_data, valid,
                                           kf_valid)
                    test_err = pred_error(f_pred, prepare_data, test, kf_test)

                    history_errs.append([valid_err, test_err])

                    if (uidx == 0 or
                        valid_err <= numpy.array(history_errs)[:,
                                                               0].min()):

                        best_p = unzip(tparams)
                        bad_counter = 0

                    print ('Train ', train_err, 'Valid ', valid_err,
                           'Test ', test_err)

                    if (len(history_errs) > patience and
                        valid_err >= numpy.array(history_errs)[:-patience,
                                                               0].min()):
                        bad_counter += 1
                        if bad_counter > patience:
                            print 'Early Stop!'
                            estop = True
                            break

            print 'Seen %d samples' % n_samples

            if estop:
                break

    except KeyboardInterrupt:
        print "Training interrupted"

    end_time = time.time()
    if best_p is not None:
        zipp(best_p, tparams)
    else:
        best_p = unzip(tparams)

    use_noise.set_value(0.)
    kf_train_sorted = get_minibatches_idx(len(train[0]), batch_size)
    train_err = pred_error(f_pred, prepare_data, train, kf_train_sorted)
    valid_err = pred_error(f_pred, prepare_data, valid, kf_valid)
    test_err = pred_error(f_pred, prepare_data, test, kf_test)

    print 'Train ', train_err, 'Valid ', valid_err, 'Test ', test_err
    if saveto:
        numpy.savez(saveto, train_err=train_err,
                    valid_err=valid_err, test_err=test_err,
                    history_errs=history_errs, **best_p)
    print 'The code run for %d epochs, with %f sec/epochs' % (
        (epoch + 1), (end_time - start_time) / (1. * (epoch + 1)))
    print >> sys.stderr, ('Training took %.1fs' %
                          (end_time - start_time))
    return train_err, valid_err, test_err


if __name__ == '__main__':
    # See function train for all possible parameter and there definition.
    ARGS = get_args()
    DATASET = "data/%s.pkl"%ARGS.DATANAME

    adv_decriptor = "adv=%s"%ARGS.ADVERSARIAL + ("" if not ARGS.ADVERSARIAL else "_eps=%s_alpha=%s"%(ARGS.ADVERSARIAL_EPSILON, ARGS.ADVERSARIAL_ALPHA))

    # a string describing the parameters of this run
    run_descriptor = '%s_%s_data=%s_maxepochs=%s_%s'%(ARGS.ENCODER, adv_decriptor, ARGS.DATANAME, ARGS.MAX_EPOCHS, util.time_string())

    MODEL_SAVETO = 'saved_models/%s.npz'%run_descriptor
    RUN_OUTPUT_FNAME = 'output/%s.out'%run_descriptor


    with open(RUN_OUTPUT_FNAME, 'w') as logfile:
        print util.colorprint("printing all standard output to %s...."%RUN_OUTPUT_FNAME, 'rand')
        sys.stdout = logfile


        # print "ARGS.ADVERSARIAL = %s"%ARGS.ADVERSARIAL
        # print "DATASET = %s"%DATASET
        # print "ARGS.MAX_EPOCHS = %s"%ARGS.MAX_EPOCHS
        # print "in summary: \n%s\n"%run_descriptor
        print "ARGS: "
        print ARGS
        print '-'*80
             
        train_lstm(
            max_epochs = ARGS.MAX_EPOCHS,
            test_size=500,
            dataset = DATASET,  
            adversarial = ARGS.ADVERSARIAL,
            adv_alpha = ARGS.ADVERSARIAL_ALPHA,
            adv_epsilon = ARGS.ADVERSARIAL_EPSILON,
            saveto = MODEL_SAVETO,
            encoder = ARGS.ENCODER
        )