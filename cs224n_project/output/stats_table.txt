LSTM_ADV (allen_adv_objective_nov15_alpha=.5_epsilon=.25.out)
default_alpha
adv_alpha=.5
adv_epsilon=.25
Valid :0.1238
Test: 0.212

LSTM_ADV (allen_adv_obj_nov16_alpha=.0_eps=.001.output)
default_alpha
adv_alpha=.9
adv_epsilon:0.001
Valid: 0.1619
Test: 0.214

LSTM_vanilla (lstm_adv=False_data=imdb_maxepochs=1000_Nov-17-2015.out)
default_param
Train  0.0 Valid  0.152380952381 Test  0.208

RNN_vanilla (rnn_vanilla_adv=False_data=imdb_maxepochs=1000_Nov-17-2015.out)
Train  0.313313313313 Valid  0.371428571429 Test  0.448

LSTM_ADV
default_LSTM
adv_alpha=.5
adv_epsilon=.5
Train  0.0 Valid  0.133333333333 Test  0.194
