#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# File: generic_util.py
# @Author: Isaac Caswell
# @created: 21 February 2015
#
#===============================================================================
# DESCRIPTION:
#
# A file containing various useful functions I often find I have to write in my 
# scripts/have to look up from other files.  For instance, how to plot things 
# with legends, print things in color, and get command line arguments
#
#===============================================================================
# CURRENT STATUS: Works!  In progress.
#===============================================================================
# USAGE:
# import generic_util as gu
# gu.colorprint("This text is flashing in some terminals!!", "flashing")
# 
#===============================================================================
# CONTAINS:
# 
#-------------------------------------------------------------------------------
# COSMETIC:
#-------------------------------------------------------------------------------
# colorprint: prints the given text in the given color
# time_string:
#       returns a string representing the date in the form '12-Jul-2013' etc.
#       Handy naming of files.
#-------------------------------------------------------------------------------
# FOR (LARGE) FILES:
#-------------------------------------------------------------------------------
# randomly_sample_file: given the name of some unnecessarily large file that you
#       have to work with, original_fname, randomly samples it to have a given
#       number of lines.  This function is used for when you want to do some 
#       testing of your script on a pared down file first.
# scramble_file_lines:
#       randomly permutes the lines in the input file.  If the input 
#       file is a list, permutes all lines in the iput files in the asme way.
#       Useful if you are doing SGD, for instance.
# file_generator:
#       streams a file line by line, and processes that line as a list of integers.
# split_file: given the name of some unnecessarily large file that you have to 
#       work with, original_fname, this function splits it into a bunch of
#       smaller files that you can then do multithreaded operations on.
#
#===============================================================================
# TODO: 
# make general plotting function


#standard modules
import numpy as np
import time
from collections import Counter, defaultdict
import heapq
import matplotlib.pyplot as plt
import argparse
import shutil
import csv
import os
import re
import collections
import json
import hashlib

#===============================================================================
# FUNCTIONS
#===============================================================================


def add_header(fname, header):
    tmp_fname = 'loltempfile'
    if header[-1] != '\n':
        header += '\n'
    shutil.copyfile(fname, tmp_fname)
    with open(fname, 'w') as f:
        f.write(header)
        with open(tmp_fname, 'r') as g:
            for line in g:
                f.write(line)
    os.remove(tmp_fname)

 
def split_file(original_fname, output_dir_fname, n_splits = 15, delimitor = '\n'):
    """given the name of some unnecessarily large file that you have to work with, original_fname,
    this function splits it into a bunch of smaller files that you can then do multithreaded 
    operations on.

    Usage: split_file('./data/words_stream.txt', './data/words_stream_split_15')

    """
    if not os.path.exists(output_dir_fname):
        os.makedirs(output_dir_fname)

    lines_in_file = 0
    with open(original_fname, 'r') as f:
        for line in f:
            lines_in_file += 1

    lines_per_subfile = lines_in_file/n_splits

    with open(original_fname, 'r') as input_f:
        cur_split = 0
        output_f = open(output_dir_fname + '/split_%s'%cur_split, 'w')
        for i, line in enumerate(input_f):
            if i%lines_per_subfile == 0 and i:
                cur_split += 1
                output_f.close()
                output_f = open(output_dir_fname + '/split_%s'%cur_split, 'w')
            output_f.write(line)

        output_f.close()


def make_dev_train_sets(original_fname, names, percents, scramble = False, preserve_header = 0):
    """
    TODO: extend to take lists of files as input. 
    """
    assert(abs(sum(percents) - 1) < 1e-5)
    assert(len(names) == len(percents))

    if scramble:
        scramble_file_lines(original_fname, original_fname  + '.scrambled', keep_first_line_first = preserve_header)
        original_fname = original_fname  + '.scrambled'

    lines_in_file = 0
    header = None
    with open(original_fname, 'r') as f:
        for i, line in enumerate(f):
            if not i and preserve_header:
                header = line
            lines_in_file += 1

    lines_per_split = [p*lines_in_file for p in percents]
    lines_per_split = [np.ceil(n) for n in lines_per_split]

    with open(original_fname, 'r') as input_f:
        cur_split = 0
        output_f = open(names[0], 'w')
        i = 0
        if preserve_header:
            input_f.readline()
        for line in input_f:
            if i%lines_per_split[cur_split] == 0 and i:
                cur_split += 1
                i=0
                output_f.close()
                output_f = open(names[cur_split], 'w')
                if preserve_header:
                    output_f.write(header)
            output_f.write(line)
            i += 1

        output_f.close()


def randomly_sample_file(original_fname, output_fname, n_lines_to_output = 100, delimitor = '\n', preserve_first_line = 1):
    """given the name of some unnecessarily large file that you have to work with, original_fname,
    randomly samples it to have n_lines_to_output.  This function is used for when you want to
    do some testing of your script on a pared down file first.

    if original_fname is a list, then all files are sampled evenly  (the same lines are taken from 
    each one.)

    Usage: 
    randomly_sample_file(["./data/features.txt", "./data/target.txt"], ["./data/dev_features.txt", "./data/dev_target.txt"], 200)

    randomly_sample_file("data/clean_mail.tsv", "data/toy.tsv")

    """
    assert(type(original_fname) == type(output_fname))
    if isinstance(original_fname, list):
        assert(len(original_fname) == len(output_fname))
    else:
        original_fname = [original_fname]
        output_fname = [output_fname]


    lines_in_file = 0
    with open(original_fname[0], 'r') as f:
        for line in f:
            lines_in_file += 1

    line_idxs_to_output = range(lines_in_file)[1:] if preserve_first_line else np.arange(lines_in_file)
    np.random.shuffle(line_idxs_to_output)
    if preserve_first_line: line_idxs_to_output = [0] + line_idxs_to_output
    line_idxs_to_output = set(line_idxs_to_output[0:n_lines_to_output])

    for input_fname_i, output_fname_i in zip(original_fname, output_fname): 
        with open(input_fname_i, 'r') as input_i, open(output_fname_i, 'w') as output_i:
            for j, line in enumerate(input_i):
                if j in line_idxs_to_output:
                    output_i.write(line)


def scramble_file_lines(original_fname, output_fname, delimitor = '\n', keep_first_line_first = 0):
    """randomly permutes the lines in the input file.  If the input 
    file is a list, permutes all lines in the iput files in the same way.
    Useful if you are doing SGD, for instance.

    Usage: 
    scramble_file_lines([X_FILENAME, Y_FILENAME], ["./data/scrambled_features.txt", "./data/scrambled_target.txt"])

    """
    assert(type(original_fname) == type(output_fname))
    if isinstance(original_fname, list):
        assert(len(original_fname) == len(output_fname))
    else:
        original_fname = [original_fname]
        output_fname = [output_fname]


    lines = [] #lines[i] is a list of lines
    headers = []
    for input_i in original_fname:
        with open(input_i, 'r') as f:
            if keep_first_line_first:
                headers.append(f.readline())
            for i, line in enumerate(f):
                if len(lines) == i:
                    lines.append([])
                lines[i].append(line)

    np.random.shuffle(lines)

    for i, output_fname_i in enumerate(output_fname): 
        with open(output_fname_i, 'w') as output_i:
            if keep_first_line_first:
                output_i.write(headers[i])
            for line in lines:
                output_i.write(line[i])

def file_generator(fname):
    """streams a file line by line, and processes that line as a list of integers."""
    with open(fname, 'r') as f:
        for line in f:
            yield [int(v) for v in line.split()]

def binarized_transcript_reader(src_filename, use_first_class=True, oversample_rate=0.0):
    """
    @param str src_filename: the filename of the tsv containing the data
    @param bool use_rand_class: if this is set to True, then each example 
    is assigned only one class, picked at random. Otherwise, a separate 
    example is output for each label.  (TODO: add commentary here about 
        advantages of each, after testing)

    @return: a generator object which yeilds a tuple (label_i, transcript), where
         transcript is a transcript string and label_i is the gold class label thereof.

    NOTE: this function requires the file be a tsv, where each line has the format:
    ID \t transcript \t [tab separated boolean vector of labels]

    NOTE 2: returns the numeric ID of the class, not the name
    Note 3: these are 1-indexed.  Blah.
    """
    class_cts = Counter()
    total_examples_seen = 0.0

    for example in csv.reader(file(src_filename), delimiter="\t"):
        total_examples_seen += 1.0
        ID, transcript = example[:2] #NOTE: ID is discarded.  Is there a reason to keep?
        classes = [i +1 for i, v in enumerate(example[2:]) if v == '1'] 
        # classes = classes[:1]
        if oversample_rate > 1 and total_examples_seen >40:
            #TODO: think through below again.  Also, it's untested.
            oversamples = []
            for cl in classes:
                # use laplace smoothing
                representedness = (class_cts[cl] +1)/((total_examples_seen +1)/len(class_cts))

                #For oversample_rate=1.0:
                # if it is overrepresented, this will add no more samples.
                # if is is represented between 1.0 and 0.5, one sample will be added.
                # if it is represented half as much as expected, this will add two more samples.
                # etc.

                oversamples += [cl]*int(oversample_rate/representedness)
            classes += oversamples

        if use_first_class:
            classes = classes[:1]

        for cl in classes:
            class_cts[cl] += 1
            yield (cl, transcript)

def load_coding_map(coding_maps_filename):
    """
    returns tuple of a dictionary from example to code (as integer), and a 
    dictionary from code int to code string
    """
    dictionary = collections.defaultdict(lambda: -1)
    codes = collections.defaultdict()
    with open(coding_maps_filename, 'r') as infile:
        mapping = json.load(infile)
        for key, val in mapping.iteritems():
            if int(key) != 0:
                code_string = val[1]
                codes[int(key)] = code_string.lower()
                examples = re.split('[;,\n]', val[2])
                for ex in examples:
                    ex = ex.strip().lower()
                    dictionary[ex] = int(key)
        return (dictionary, codes)
    return None


#-----------------------------------------------------------------------------------------            


def colorprint(message, color="rand", newline=True):
    """
    '\033[%sm' + message +'\033[0m',
    """
    message = str(message)
    if color == "bare":
        print message;
        return
    # message = unicode(message)
    """prints your message in pretty colors! So far, only a few color are available."""
    if color == 'none': print message,
    if color == 'demo':
        for i in range(-8, 109):
            print '\n%i-'%i + '\033[%sm'%i + message + '\033[0m\t',
        return
    print '\033[%sm'%{
        'bold' : 1,
        'grey' : 2,
        'underline' : 4,
        'flashing' : 5,
        'black_highlight': 7,
        'invisible' : 8,
        'black' : 30,
        'purple' : 34,      
        'peach' : 37,
        'red_highlight' : 41,
        'green_highlight' : 42,
        'orange_highlight' : 43,
        'blue_highlight' : 44,
        'magenta_highlight' : 45,
        'teal_highlight' : 46,
        'lavendar_highlight' : 47,        
        'graphite' : 90,
        'red' : 91,
        'green' : 92,
        'yellow' : 93,  
        'blue' : 94,
        'magebta' : 95,
        'teal' : 96,
        'pink' : 97,
        'neutral' : 99,
        'rand' : np.random.choice(range(90, 98) + range(40, 48)+ range(30, 38)+ range(0, 9)),
    }.get(color, 1)  + message + '\033[0m',
    if newline:
        print '\n',


def print_matrix(mat, delimitor="\t", precision=3, color="bare", newline=True):
    """
    prints the elements in a matrix in a nice way!
    """
    assert len(mat.shape) <= 2
    if len(mat.shape) == 1:
        mat = mat.reshape((1, mat.shape[0]))
    res = ""
    val_format = "%." + str(precision) + "f"
    for row in mat:
        res += delimitor.join(val_format%val for val in row)
        res += "\n"
    colorprint(res, color, newline=newline)

    

def madness(delay=3):
    import threading
    import os
    def speaker():
        try:
            os.system("osascript -e \"set Volume 5\"")
            os.system("sleep %s"%delay)
            os.system("say -v whisper -r 1 Oh God &")
            os.system("sleep 3")
            os.system("osascript -e \"set Volume 3\"")
            os.system("say -v whisper Oh God &")
            os.system("sleep 2")
            os.system("say -v whisper help &")
            os.system("sleep 1")
            os.system("osascript -e \"set Volume 7\"")
            os.system("say -v veena will someone help him? &")
            os.system("sleep 4")

            os.system("osascript -e \"set Volume 5\"")
            os.system("say -v kyoko こんにちは、私の名前はKyokoです。日本語の音声をお届けします &")
            os.system("say -v tarik مرحبًا اسمي Tarik. أنا عربي من السعودية &")
            os.system("sleep 1")
            os.system("say -v lekha What kind of balls, does dees man have.")

            os.system("say -v whisper oh god Oh God Oh God &")
            os.system("say -v veena oh god.  oh god &")
            os.system("sleep 20")
            os.system("osascript -e \"set Volume 5\"")
            os.system("say -v whisper please &")
            os.system("sleep 1")
            os.system("say -v tarik مرحبًا اسمي &")
            # os.system("sleep 200")
            os.system("osascript -e \"set Volume 2\"")
            # os.system("say -v veena its friday friday friday ooh.  Gotta get down on friday &")
            # os.system("say -v tarik مرحبًا اسمي &")
            os.system("sleep 200")
            os.system("osascript -e \"set Volume 10\"")
            os.system("say -v milena Союз нерушимый республик свободных Сплотила навеки Великая Русь! Да здравствует созданный волей народов Единый, могучий Советский Союз! Славься, Отечество наше свободное, Дружбы народов надёжный оплот! Партия Ленина — сила народная Нас торжеству коммунизма ведёт! Сквозь грозы сияло нам солнце свободы, И Ленин великий нам путь озарил: На правое дело он поднял народы, На труд и на подвиги нас вдохновил! В победе бессмертных идей коммунизма Мы видим грядущее нашей страны, И Красному знамени славной Отчизны Мы будем всегда беззаветно верны! &")              
            import webbrowser as wb
            os.system("sleep 5")
            wb.open("https://www.youtube.com/watch?v=x72w_69yS1A")


        except:
            pass

    th = threading.Thread(target=speaker)
    th.start()

def time_string(precision='day'):
    """ returns a string representing the date in the form '12-Jul-2013' etc.
    intended use: handy naming of files.
    """
    t = time.asctime()
    precision_bound = 10 #precision == 'day'
    yrbd = 19
    if precision == 'minute':
        precision_bound = 16
    elif precision == 'second':
        precision_bound = 19
    elif precision == 'year':
        precision_bound = 0
        yrbd = 20
    t = t[4:precision_bound] + t[yrbd:24]
    t = t.replace(' ', '-')
    return t


def random_string_signature(length = 4):
    candidates = list("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890")
    np.random.shuffle(candidates)
    return "".join(candidates[0:length])

def str_parse(s, to_lower = True):
    parse = re.split('[\s/\\\\,]', s)
    # below line fails on unicode
    # parse = [w.translate(string.maketrans("",""), string.punctuation) for w in parse]
    parse = [re.sub('[,<>\.;:!?\"]', '', w) for w in parse]
    parse = [w for w in parse if w]
    if to_lower:
        parse = [w.lower() for w in parse]
    return parse


def descriptive_filename(attributes, 
                         omit={},
                         id="", 
                         truncate_decimals=0, 
                         shorten=False, 
                         timestamp_precision='day', 
                         random_stamp=False, 
                         extension = '.out'
                         ):
    """
    produces a nice, descriptive filename for a dict-like that specifies the attributes of a run.
    Also offers the ability to add a random descriptor to prevent filename collisions, and the 
    option to shorten it to something less human readable but less annoying.

    EXAMPLES:
    -------------------------------------------------------------------------------
    # default
    alpha=0.5_optimizer=rmsprop_nu=1.6e-11_epsilon=0.001__Dec-10-2015.out 

    # adding id "toy-test" and truncating decimals to three decimal places
    toy-test__alpha=0.500_optimizer=rmsprop_nu=0.000_epsilon=0.001__Dec-10-2015.out

    # adding id "toy-test" and omitting the parameter "epsilon"
    toy-test__alpha=0.5_optimizer=rmsprop_epsilon=0.001__Dec-10-2015.out

    # adding id "toy-test" and using the option shorten=True
    94dd2__Dec-10-2015.out

    # iusing a random identifier and a custom extension ".lol"
    alpha=0.5_optimizer=rmsprop_nu=1.6e-11_epsilon=0.001__xIFm__Dec-10-10:07:04-2015.lol
    -------------------------------------------------------------------------------    

    :param dict attributes: a dict OR an argparse.Namespace that maps parameter names to their values 
    :param iterable omit: an iterable specifying parameters to exclude from the filename
    :param str id: an id (e.g. to identify a particular set of tests) that will be prepended to the result
    :param int truncate_decimals: if this is nonzero, all floats mapped to by attributes will be truncated 
            at this decimal count.  The problem with this is that numbers like 1e-6 will turn into 
            e.g. 0.000, and 5.0 will turn into 5.000
    :param bool shorten: if True, the filename will be a hash of the string describing the attributes, 
            rather than the string itself.  The reason for this is that if you have a lot of attributes,   
            the filename could get maddeningly long.  timestamp is still added after this.
    :param str timestamp_precision: the precision of the timestamp.  May be "day", "minute" or "second".  
            If it is False or "", no timestamp is appended to the filename.
    :param bool random_stamp: whether to add a random stamp at the end of the filename.  This should be 
            used if multiple runs with the same parameters want different filenames (e.g. so the results
            of several runs are averaged)
    :param str extension: what extension to use for the file.  Must begin in '.' or be the empty string 
            (signifying no extension)

    """
    if isinstance(attributes, argparse.Namespace):
        attributes = dict(attributes._get_kwargs());

    res = "" if not id else id+"__"
    #--------------------------------------------------
    # get the identifier from the attribute dict
    # 
    complete_identifier = ""
    for key, val in attributes.items():
        if key in omit: continue
        strval = str(val)
        if truncate_decimals and isinstance(val, float):
            strval = ("%.{0}f".format(truncate_decimals))%(val)
        complete_identifier += "%s=%s_"%(key, strval)
    complete_identifier = complete_identifier[0:-1] #remove trailing underscore
    if shorten:
        hashed_id = hashlib.sha1(complete_identifier).hexdigest()
        complete_identifier = str(hashed_id)[0:5]
    res += complete_identifier
    #--------------------------------------------------
    # add random stamp and timestamp, if wanted
    if random_stamp:
        res += '__' + random_string_signature(length=4)
    if timestamp_precision:
        res += '__' + time_string(timestamp_precision)

    if extension:
        assert extension[0] == '.'
        res += extension


    return res

    


class BarGraph:
    '''
        Module to allow for bar graphs

    '''
    
    def __init__(self, title, ylabel='', xlabel=''):
        self.clfs_to_f1 = defaultdict(list)
        self.clfs = []
        self.datasets = []
        self.title = title
        self.ylabel = ylabel
        self.xlabel = xlabel
        self.width = 0.08
        self.colors = ('b','g','r','c','m','y','k','w')


    def add_to_graph(self, dataset_name, clf_name, f1):
        self.clfs_to_f1[clf_name].append(f1)
        if clf_name not in self.clfs:
            self.clfs.append(clf_name)
        if dataset_name not in self.datasets:
            self.datasets.append(dataset_name)

    def plot(self):
        ind = np.arange(len(self.datasets))
        fig, ax = plt.subplots()
        rects = [ax.bar(ind+self.width*i, self.clfs_to_f1[clf], self.width, color=self.colors[i%len(self.colors)]) for i, clf in enumerate(self.clfs)]
        ax.set_ylabel(self.ylabel)
        ax.set_title(self.title)
        ax.set_xticks(ind+self.width*len(self.clfs))
        ax.set_xticklabels( tuple(self.datasets) )
        ax.legend(tuple([rect[0] for rect in rects]), tuple(self.clfs))
        plt.show()

    def sklearn_clf_rpt_parser(self, rpt):
        line = re.findall('avg / total [\.\d\s]*', rpt)
        if line == []:
            return ()
        return float(line[0].split()[5]) 


def make_toy_data(outfile, dim=5, n_examples=100, misclassification_prob=0.1, predictor="dot", seeds=None):
    """
    creates a file of artificial data, where each line is an example (space separated values) 
    followed by a tab and then the true label

    :param string outfile: the file to which to write the data
    :param int dim: the dimensionality of the training examples that are to be made
    :param int n_examples: the number of examples to be made
    :param float misclassification_prob: this number of examples will be intentionally misclassified.
            right now this does not actually work--I just take the negative of the vector (works for dot but little else)
    :param function or str hypothesis: either a predictor that takes as input a numpy vector and 
    :param tuple seeds: a tuple of 0. the seed used to create the weight vectors, and 1. the seed for the examples.
            The reason for this is so that we can make different datasets (e.g. train and test) with the same weights
    returns an int or string, or a string representing one of our pre-built predictors.
    """
    if seeds: 
        np.random.seed(seeds[0])
    if isinstance(predictor, str):
        w = 2*(np.random.random((dim,)) - 0.5) # used for dot and pathological
        w2 = 2*(np.random.random((dim,)) - 0.5) # used only in pathological
        if predictor=="dot":
            predictor = lambda x: 1 if x.dot(w) > 0 else 0
        elif predictor=="sigmoid":
            predictor = lambda x: 1 if 1.0/(1.0 + np.exp(x.dot(w))) > 0.5 else 0
        elif predictor=="pathological":
            def p(x):
                a = np.log(np.outer(x, x + w2)**2).dot(x)
                a += x.dot(x**2 + w)
                # a **= 1.3
                a = a.dot(w)
                return 1 if a > -2 else 0 
            predictor = p
        else:
            print "please specify 'sigmoid' or 'dot'"
            return
    if seeds: 
        np.random.seed(seeds[1])            
    with open(outfile, "w") as f:
        labels = []
        for _ in range(n_examples):
             x = 2*(np.random.random((dim,)) - 0.5)
             y = predictor(x)
             labels.append(y)
             if np.random.rand() < misclassification_prob:
                x *=-1
             line_to_write = " ".join(str(val) for val in x) + "\t" + str(y) + "\n"
             f.write(line_to_write)
    print "class balance: ", Counter(labels)

def cells_from_shape(shape):
    """
    given the shape of an array, e.g. (2,3,18), provides an iterator to go over all cells in that array
    (e.g. (0,0,0), (0,0,1), ....(1,2,17))
    """
    index_total = reduce(lambda x, y: x*y, shape, 1)
    stride_sizes = [1]
    for dim in reversed(shape[1:]):
        stride_sizes = [dim*stride_sizes[0]] + stride_sizes

    for i in range(index_total):
        res = []
        for stride in stride_sizes:
            res.append(i/stride)
            i -= stride*res[-1]
        yield tuple(res)
    # for axis, dim in enumerate(shape):
    #     for i in range(dim):

def check_for_wonkiness(obj, verbose=1):
    """
    takes an object and runs a series of simple diagnostics to see if any number of common things are up with it.
    returns True if the object is wonky.
    """

    if isinstance(obj, float):
        if np.isnan(obj):
            if verbose>=1:
                print "it's NaN!"
                return True
        if np.isinf(obj):
            if verbose>=1:
                print "it's inf!"            
                return True
    return False                


#===============================================================================
# TESTING SCRIPT
#===============================================================================

#-------------------------------------------------------------------------------
# part (i) 


if __name__ == '__main__':
    # print 'You are running this from the command line, so you must be demoing it!'


    # print str_parse("//THERE REALLY HASN'T BEEN ONE//ILLEGAL IMMIGRATION")
    # print str_parse("<DK>")
    # print str_parse("\\\\jobs\\\\ anyting else\\\\")  

    # print str_parse("service, installs systems into houses, condos, hotels")         
    # print str_parse("the war/so many deaths/")         
    # print str_parse("Finance/everybody is going broke, all small businesses are shuting down,big corporations are letting go of workers")                 
    # print str_parse(""""jobs"//""")

    # pc = PlotCatcher()
    # # exit(0)
    # def report_smartness(): 
    #     if np.random.random()<.8 :
    #         return 'still training...' 
    #     else:
    #         return """FINDINGS ON DATASET ./preprocessed_data/train/mip_personal_2_binarized_pp_train.tsv:
    #          precision    recall  f1-score   support
    #         avg / total       0.74      0.75      0.73      2009
    #         Accuracy: 0.749128919861"""

    # for i in range(19):
    #     print pc.catch(i**2, 'x squared')
    #     print pc.catch(i**3, 'x cubed', c='r--')  
    #     print pc.catch(report_smartness(), 'ml', pc.sklearn_clf_rpt_parser)      
    # pc.plotByIds(['x squared', 'x cubed'])
    # pc.plotByIds('ml')


    print "NOW FOR A DEMO OF HOM YOU CAN MAKE NICE, HYPERDESCRIPRIVE FILENAMES FOR YOUR MILLION RUNA OF THAT SCRIPT"
    run_attributes = {"alpha": 0.5, "epsilon": 0.001, "optimizer": "rmsprop", "nu": 16e-12}
    omit = {"nu"}
    print descriptive_filename(run_attributes)
    print descriptive_filename(run_attributes, id="toy-test", truncate_decimals=3)
    print descriptive_filename(run_attributes, id="toy-test", omit=omit)    
    print descriptive_filename(run_attributes, shorten=True)
    print descriptive_filename(run_attributes, timestamp_precision="second") 
    print descriptive_filename(run_attributes, timestamp_precision="second", random_stamp=True, extension=".lol")

    parser = argparse.ArgumentParser()
    parser.add_argument('-stuff', type=str, default="yeah")
    ARGS = parser.parse_args()
    print descriptive_filename(ARGS)     



    # colorprint("nyaan", color="demo")           

    # exit(0)
    # print os.listdir('../original_data/all_data')
    # fnames = ['current_industry', 'current_occupation', 'MIP_personal_1', 'MIP_personal_2', 'MIP_political_1', 'MIP_political_2', 'past_industry', 'past_occupation', 'PK_brown', 'PK_cheney', 'PK_pelosi', 'PK_roberts', 'terrorists']
    # fnames = ['current_industry_binarized', 'current_occupation_binarized', 'mip_personal_binarized', 'mip_political_binarized', 'past_industry_binarized', 'past_occupation_binarized', 'pk_brown_binarized', 'pk_cheney_binarized', 'pk_pelosi_binarized', 'pk_roberts_binarized', 'terrorists_binarized']
    # fnames =  ['industry_binarized', 'occupation_binarized']
    # for fname in fnames:
    # fname = "mip_binarized"
    # make_dev_train_sets('../original_data/all_data/%s.tsv'%fname, \
    #     ['../original_data/train/%s_train.tsv'%fname, 
    #     '../original_data/dev/%s_dev.tsv'%fname, 
    #     '../original_data/test/%s_test.tsv'%fname
    #     ], 
    #     [.75, .15, .1], 
    #     scramble = True)
    # add_header('data/clean_mail_test.tsv', 'Subject   Delivered-To    Received    From    To  X-GM-THRID  X-Gmail-Labels  importance  content')
    # add_header('data/clean_mail_train.tsv', 'Subject   Delivered-To    Received    From    To  X-GM-THRID  X-Gmail-Labels  importance  content')
    # randomly_sample_file("data/clean_mail.tsv", "data/toy_train.tsv", preserve_first_line = 1)
    # randomly_sample_file("data/clean_mail.tsv", "data/toy_test.tsv", preserve_first_line = 1)








