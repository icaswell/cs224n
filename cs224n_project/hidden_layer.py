#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# File: hidden_layer.py
# @Author: Isaac Caswell/whomever wrote the theano tutorial
# @created: 29 Oct 2015
#
#===============================================================================
# DESCRIPTION:
#
# A class representing a hidden layer in a deep network. Based heavily off the theano
# tutorial (http://deeplearning.net/tutorial/mlp.html#mlp)
#
#===============================================================================
# CURRENT STATUS: unimplemented (29 Oct 2015)
#===============================================================================
# USAGE:
# [unimplemented]
# 
#===============================================================================
# THEORETICAL OVERVIEW: (largely copypasted from the tutorial)
#-------------------------------------------------------------------------------
#===============================================================================
# TODO: 
# -everything

import numpy as np

import theano
import theano.tensor as T


class HiddenLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                 activation=T.tanh):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        tanh activation function by default. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        Hidden unit activation is given by: activation(dot(input,W) + b)

        :type rng: np.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer

        PICTORIALLY:
        ☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭☭
        -----------------------------------------------------------
        self.output-->   A  A  A  A  A  A
        activation-->    |  |  |  |  |  |
        lin_output-->    O  O  O  O  O  O           [n_out]
                          \ |\/|\/|\/| / \ 
        W -->              \|/\|/\|/\|/   \ 
        input[0, :]-->      O  O  O  O     b        [n_in]
                                
        ♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞♞
        """
        if activation is None:
        	print "please specify a non-None activation!"
        	exit(0)

        self.input = input
        # `W` is initialized with `W_values` which is uniformely sampled
        # from sqrt(-6./(n_in+n_hidden)) and sqrt(6./(n_in+n_hidden))
        # for tanh activation function
        # the output of uniform if converted using asarray to dtype
        # theano.config.floatX so that the code is runable on GPU
        # Note : optimal initialization of weights is dependent on the
        #        activation function used (among other things).
        #        For example, results presented in [Xavier10] suggest that you
        #        should use 4 times larger initial weights for sigmoid
        #        compared to tanh
        #        We have no info for other function, so we use the same as
        #        tanh.
        # TODO:  change below if we decide to use, say, a ReLU
        if W is None:
            W_values = np.asarray(
                rng.uniform(
                    low=-np.sqrt(6. / (n_in + n_out)),
                    high=np.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )
            if activation == theano.tensor.nnet.sigmoid:
                W_values *= 4


            W = theano.shared(value=W_values, name='W', borrow=True)

        if b is None:
            b_values = np.zeros((n_out,), dtype=theano.config.floatX)
            b = theano.shared(value=b_values, name='b', borrow=True)

        self.W = W
        self.b = b
		
		#Note that we used a given non-linear function as the activation function of the hidden layer.
        lin_output = T.dot(input, self.W) + self.b
        self.output = activation(lin_output)
        # parameters of the model
        self.params = [self.W, self.b]
