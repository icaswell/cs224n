#!usr/bin/bash

# THEANO_FLAGS=optimizer=fast_compile,exception_verbosity=high python rnn.py --wdim 50 --hdim 60 --epochs 2 --data toy_corpus --wemb_init random --maxlen 20 --clip 1.0 $*
THEANO_FLAGS=optimizer=fast_compile,exception_verbosity=high python rnn.py --wdim 300 --hdim 54 --epochs 2 --data toy_corpus --wemb_init word2vec --maxlen 20 --clip 1.0 $*
