#!usr/bin/bash

THEANO_FLAGS=device=gpu4,floatX=float32 nohup python rnn.py --id gpu_4 --redirect  --optimizer adadelta --clip 5 --wdim 300 --batch_size 20 --encoder lstm --epochs 1000 --lrate 0.001 --data imdb --hdim 300 --weight-init ortho_0.463825086526 --maxlen 100 --reg 0.01 \
--adv 1 --alpha 0.275880459498 --eps 0.0182560015146 &
