#!/usr/bin/bash
# a wrapper to take arguments and pass them to the python script
source deeplearning_env/bin/activate
DEVICE=$1
THEANO_FLAGS=device=gpu$DEVICE,floatX=float32 python rnn.py "${@:2}"
