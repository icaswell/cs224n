# convert_text_corpus_to_ince_pickle.py


#infile should be a 
import cPickle
import os
import re
from nltk.tokenize import word_tokenize

# format: \n separated data, with a \tlabel in the end
INFILE = "./data/toy_corpus.txt" 
INFILE_LINE_DELIMITOR = "\n"
INFILE_LABEL_DELIMITOR = "\t"
DATA_OUTFILE = "./data/toy_corpus.pkl"
IDX_MAP_OUTFILE = "./data/toy_corpus_idx_map.pkl"

USE_GLV_INDICES = False

assert(not USE_GLV_INDICES)


#--------------------------------------------------

def readlines_by_delimitor(f, delim):
    """
    from 
    http://stackoverflow.com/questions/16260061/reading-a-file-with-a-specified-delimiter-for-newline

    might like a better alternative
    """
    buf = ""
    while True:
      while delim in buf:
        pos = buf.index(delim)
        yield buf[:pos]
        buf = buf[pos + len(delim):]
      chunk = f.read(4096)
      if not chunk:
        yield buf
        break
      buf += chunk


#--------------------------------------------------

numericized_data = []
labels = []

word_to_idx = {}
label_to_idx = {}

max_word_idx = 0
max_label_idx = 0

with open(INFILE, "r") as f:
    for line in readlines_by_delimitor(f, INFILE_LINE_DELIMITOR):
        line = line.strip()
        datum, label = re.split(INFILE_LABEL_DELIMITOR, line)

        if label not in label_to_idx:
            label_to_idx[label] = max_label_idx
            max_label_idx += 1
        labels.append(label_to_idx[label])

        datum_string_list = word_tokenize(datum);
        datum_idx_list = []
        for word in datum_string_list:
            if word not in word_to_idx:
                word_to_idx[word] = max_word_idx
                max_word_idx += 1
            datum_idx_list.append(word_to_idx[word])
        numericized_data.append(datum_idx_list)

with open(DATA_OUTFILE, 'wb') as outfile:
    tup = (numericized_data, labels)
    cPickle.dump(tup, outfile, protocol=cPickle.HIGHEST_PROTOCOL)


with open(IDX_MAP_OUTFILE, 'wb') as outfile:
    tup = (word_to_idx, label_to_idx)
    cPickle.dump(tup, outfile, protocol=cPickle.HIGHEST_PROTOCOL)

if 0:
    f = open(DATA_OUTFILE, 'rb')
    train_set = cPickle.load(f)
    f.close()




