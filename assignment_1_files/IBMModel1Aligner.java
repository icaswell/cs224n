package cs224n.wordaligner;

import cs224n.util.*;
import java.util.List;
import java.util.HashSet;

/**                                                                                                                                                                                  
 * Simple word alignment baseline model that maps source positions to target                                                                                                         
 * positions along the diagonal of the alignment grid.                                                                                                                               
 *                                                                                                                                                                                   
 * IMPORTANT: Make sure that you read the comments in the                                                                                                                            
 * cs224n.wordaligner.WordAligner interface.                                                                                                                                         
 *                                                                                                                                                                                   
 * @author Dan Klein                                                                                                                                                                 
 * @author Spence Green                                                                                                                                                              
 */

public class IBMModel1Aligner implements WordAligner {

  private static final long serialVersionUID = 1315751943476440515L;

  // TODO: Use arrays or Counters for collecting sufficient statistics                                                                                                               
  // from the training data.                                                                                                                                                         
  //************************************************************
  // variables:

  //t(f|e)
  //AKA sourceTargetCounts
  //first argument is the target word, second the source
  private CounterMap <String, String> p_tgt_given_src = new CounterMap<String,String>(); 

    //************************************************************

  public Alignment align(SentencePair sentencePair) {
    // Placeholder code below.                                                                                                                                                       
    // TODO Implement an inference algorithm for Eq.1 in the assignment                                                                                                              
    // handout to predict alignments based on the counts you collected with train().                                                                                                 
    Alignment alignment = new Alignment();

    // YOUR CODE HERE                                                                                                                                                                
    List<String> sourceWords = sentencePair.getSourceWords();
    List<String> targetWords = sentencePair.getTargetWords();
    targetWords.add("<NULL>");
    for(int srcIdx = 0; srcIdx < sourceWords.size(); srcIdx++){
        String srcWord = sourceWords.get(srcIdx);
        int bestTransIdx = -1;
        double bestTransIdxVal = -1.0;

        //get the target word yielding the highest translation probability... 
        for(int tgtIdx = 0; tgtIdx < targetWords.size(); tgtIdx++){
            String tgtWord = targetWords.get(tgtIdx);            
            if (this.p_tgt_given_src.getCount(tgtWord, srcWord) > bestTransIdxVal){
                bestTransIdx = tgtIdx;
                bestTransIdxVal = this.p_tgt_given_src.getCount(tgtWord, srcWord);
                if (tgtWord.equals("<NULL>")){
                    bestTransIdx = -1;
                }
            }
        }
        //and add the relevant index to the alignment
        if(bestTransIdx != -1){
            // don't add the NULL alignment
            alignment.addPredictedAlignment(srcIdx, bestTransIdx);
        }
    }

    return alignment;
    }

    public void train(List<SentencePair> trainingPairs) {

        //************************************************************
        //int src_vocab_size = 0;
        int tgt_vocab_size = 0;
	HashSet<String> tgtVocab = new HashSet<String>();
	HashSet<String> srcVocab = new HashSet<String>();
	//TODO: make set of sourcewords, targetwords to iterate over
        for (SentencePair sentencepair:trainingPairs){
	    List<String> sourceWords = sentencepair.getSourceWords();
	    List<String> targetWords = sentencepair.getTargetWords();
	    for (String word:sourceWords){
		srcVocab.add(word);
	    }
	    for (String word:targetWords){
		tgtVocab.add(word);
	    }
	    //src_vocab_size += sourceWords.size();
	    tgt_vocab_size += targetWords.size();                
        }

        // TODO: which of the above constants to normalize by?
        double uniform_prob = 1.0/tgt_vocab_size;
        
        for (int i=0; i < 100; i++){
            //TODO: make this actually run until convergence

            // alignment_counts(srcWord, tgtWord) gives the pseudoprobability that tgtWord and srcWord are aligned.
            // AKA the (fractional) co-occurence counts
            // NOTE THAT THE ORDER OF ARGUMENTS IS REVERSED FROM p_tgt_given_src!
            // TODO: I want to make these on the stack! how can I avoid 'new'?
            CounterMap <String, String> alignment_counts = new CounterMap<String, String>(); 
            Counter <String> src_counts = new Counter<String>();

            //========================================================
            // E-step : apply the model to the data.  
            // This means: get the most likely alignments
            // This means: calculate the probabilities of all m*l alignments,
            // by means of calculating, for each target word, the alignment probability of each source word
            for (SentencePair sentencepair:trainingPairs){
                List<String> sourceWords = sentencepair.getSourceWords();
                List<String> targetWords = sentencepair.getTargetWords();
                targetWords.add("<NULL>");

                //the sum of the counts of each target word.  Used for normalization
                //TODO: I want this on the stack!
                Counter<String> target_sums = new Counter<String>();
                //get normalization constants/populate (target_sums)
                for (String tgtWord:targetWords){
                    for (String srcWord:sourceWords){
                        double p =  p_tgt_given_src.getCount(tgtWord,srcWord); //rename p
                        target_sums.incrementCount(tgtWord, p);
                    }
                }

                for (String tgtWord:targetWords){
                    for (String srcWord:sourceWords){
			double prob = this.p_tgt_given_src.getCount(tgtWord, srcWord);
			if (prob == 0.0) prob = uniform_prob; 
                        double count_delta = prob/target_sums.getCount(tgtWord);
                        alignment_counts.incrementCount(srcWord, tgtWord, count_delta);
                        src_counts.incrementCount(srcWord, count_delta);
                    }
                }
            }
            //========================================================        
            // M-step: learn the model from the data/Renormalization
            //this means, one presumes, estimating t(f|e) for all f
	    //run over all pairs of (src_word, tgt_word) in
            // alignment_counts
	    for (String srcWord:srcVocab){
		for (String tgtWord:tgtVocab){
		    double joint = alignment_counts.getCount(srcWord, tgtWord);
		    double prior = src_counts.getCount(srcWord);
		    this.p_tgt_given_src.setCount(tgtWord, srcWord, joint/prior);
		}
	    }
        }                                                                                                                                                          
    }
}




